class ClothSize{


	public static void main(String[] args){

		String s = "S";

		if(s == "S"){

			System.out.println("Shirt is type of small size");
		}
		else if(s == "M"){

			System.out.println("Shirt is type of medium size");
		}
		else if(s == "L"){

			System.out.println("Shirt is type of large size");
		}
		else if(s == "XL"){

			System.out.println("Shirt is type of extra large size");
		}
		else if(s == "xxl"){

			System.out.println("Shirt is type of double extra large size");
		}else{
	
			System.out.println("You are out of size!");
		}
	}
}
