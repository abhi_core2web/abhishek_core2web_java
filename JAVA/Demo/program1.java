class Outer {

    void fun() {
        System.out.println("In fun");

        class Inner1 {
            Inner1() {
                System.out.println("In Inner1-Constructor.");

                // Creating an anonymous class to affect the numbering of subsequent nested classes
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("In Anonymous Runnable.");
                    }
                };
                r.run();

                class Inner2 {
                    Inner2() {
                        System.out.println("In Inner2-Constructor.");
                    }
                }

                // Create an instance of Inner2 inside the Inner1 constructor
                Inner2 inner2 = new Inner2();
            }
        }

        // Create an instance of Inner1 inside the fun method
        Inner1 obj = new Inner1();
    }

    public static void main(String[] args) {
        Outer obj = new Outer();
        obj.fun();
    }
}

