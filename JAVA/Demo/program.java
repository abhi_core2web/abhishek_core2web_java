class Outer {

	void fun() {

		System.out.println("In fun");

		class Inner1 {

			Inner1() {

				System.out.println("In Inner1-Constructor.");
			
				class Inner2 {

					Inner2() {
						System.out.println("In Inner2-Constructor. ");
					}
				}

				Inner2 obj = new Inner2();
			}

			Inner1 obj1 = new Inner1();
		}
	}

	public static void main(String[] args){

		Outer obj = new Outer();
		obj.fun();
	}
}
