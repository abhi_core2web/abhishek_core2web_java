import java.util.*;

class HappyNumber{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number: ");
		int cnt =0;
		System.out.print("[");
		for(int i=1; i<=1000; i++){
			int num = i;
			int sum = 0;
			int tmp = num;

			while(true){
				sum = 0;
				while(num!=0){
					int rem = num % 10;
					int sqr = rem * rem;
					sum += sqr;	
					num /= 10;
				}
		
				if(sum==1){
	
					//System.out.println(tmp+" Number is Happy Number.");
					System.out.print(tmp+", ");
					cnt++;
					break;
				}else if(sum==tmp || sum<10){
			
					//System.out.println(tmp+" Number is not Happy number");
					break;
				}
				num = sum;
			}
		}
		System.out.print("]\n");
		System.out.println("Total Happy numbers from 1 to 1000 are :"+cnt);
	}
}

