/*
WAP to take two strings input from the user and check whether those string objects are equal or not if equal print true else false
*/

import java.util.*;

class EqualDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter First String :");
		String str1 = new String(sc.nextLine());
		System.out.println("Enter Second String :");
		String str2 = new String(sc.nextLine());
	
		System.out.println(str1.equals(str2));
	}
}
