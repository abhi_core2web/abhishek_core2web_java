/*
Take two input strings str1 & str2 from the user and print 0 if both strings are equal else print the difference between unequal element.
*/

import java.util.*;

class CompareToDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter First String :");
		String str1 = new String(sc.nextLine());
		System.out.println("Enter Second String :");
		String str2 = new String(sc.nextLine());
	
		System.out.println(str1.compareTo(str2));
	}
}
