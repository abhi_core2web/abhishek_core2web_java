/*
 WAP to take input as a string from the user and print all the characters one by one.
*/

import java.util.*;

class CharAtDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
	
		System.out.println("One by One Character :");	
		for(int i=0; i<str.length(); i++){

			System.out.println(str.charAt(i));
		}
	}
}
