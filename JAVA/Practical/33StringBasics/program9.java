/*
WAP to remove void spaces in the string
*/

import java.util.*;

class ReplaceDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
		
		System.out.println("Replaced String :"+str.replace('a','e'));
	}
}
