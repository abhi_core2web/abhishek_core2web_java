/*
Q6.
Write a Java program that compares two strings without considering their case
sensitivity.
*/

import java.util.*;

class EqualIgnoreCaseDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter First String :");
		String str1 = new String(sc.nextLine());
		System.out.println("Enter Second String :");
		String str2 = new String(sc.nextLine());
	
		System.out.println(str1.equalsIgnoreCase(str2));
	}
}
