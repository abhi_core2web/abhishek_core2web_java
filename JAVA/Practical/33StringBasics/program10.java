/*
WAP to check if the string is empty if not then print the last character of the string.Take string input from the user.
*/

import java.util.*;

class StringDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
		
		if(str.length() == 0){

			System.out.println("String is Empty.");
		}else{	
			System.out.println("Last character form string is :"+str.charAt(str.length()-1));
		}
	}
}
