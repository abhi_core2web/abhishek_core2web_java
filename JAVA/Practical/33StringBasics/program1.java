/*
 WAP to print user given string using Scanner class.
*/

import java.util.*;

class StringDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
		System.out.println("Entered String is :"+str);
	}
}
