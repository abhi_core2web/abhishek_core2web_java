/*
WAP to remove void spaces in the string
*/

import java.util.*;

class TrimDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
		
		System.out.println("Trimed  String :"+str.trim());
	}
}
