/*
WAP to convert all alphabets in the string to uppercase.Take string from user.
*/

import java.util.*;

class UpperCaseDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String :");
		String str = new String(sc.nextLine());
		
		System.out.println("Upper case String of given String :"+str.toUpperCase());
	}
}
