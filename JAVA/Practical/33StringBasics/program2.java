/*
 * WAP to concat the strings given by the user and count the length of string after Concatenation.
*/

import java.util.*;

class ConcateDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first String :");
		String str1 = new String(sc.nextLine());
		System.out.println("Enter Second String :");
		String str2 = new String(sc.nextLine());

		String str3 = str1.concat(str2);
		System.out.println("After Concatination String is "+str3+" with length "+str3.length());
	}
}
