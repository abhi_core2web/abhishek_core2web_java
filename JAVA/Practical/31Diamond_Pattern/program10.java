import java.util.*;

class Pattern10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Rows: ");
        int rows = sc.nextInt();
        int col = 0;
        int s = 0;

        for (int i = 1; i <= rows * 2 - 1; i++) {
            int num = 1;
            if (i <= rows) {
                col = i * 2 - 1; // 1,3,5,7...
                s = rows - i;
            } else {
                col -= 2; // 5,3,1...
                s = i - rows;
            }

            for (int sp = 1; sp <= s; sp++) {
                System.out.print("\t");
            }
            for (int j = 1; j <= col; j++) {
                if (j < col / 2 + 1) {
                    if (j % 2 == 1) {
                        System.out.print(num + "\t");
                        num++;
                    } else {
                        System.out.print((char) (64 + num) + "\t");
                    }
                } else {
                    if (j % 2 == 1) {
                        System.out.print(num + "\t");
                        num--;
                    } else {
                        System.out.print((char) (64 + num) + "\t");
                    }
                }
            }
            System.out.println();
        }
    }
}

