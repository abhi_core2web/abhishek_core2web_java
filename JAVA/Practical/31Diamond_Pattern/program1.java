/*Q1.Take row input from the user.
row=4

			#
		# 	# 	#
	# 	# 	# 	# 	#
# 	# 	# 	# 	# 	# 	#
	# 	# 	# 	# 	#
		# 	# 	#
			#

row=3

		#
	# 	# 	#
# 	# 	# 	# 	#
	# 	# 	#
		#
*/

import java.util.*;

class Pattern1{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int col = 0;
		int s = 0;

		for(int i=1; i<rows*2; i++){

			if(i<=rows){

				col = i*2-1;
				s = rows-i;
			}else{

				col -= 2;
				s = i-rows;
			}

			for(int sp=1; sp<=s; sp++){

				System.out.print("\t");
		
			}
			for(int j=1; j<=col; j++){

				System.out.print("*\t");
			}
			System.out.println();
		}	
	}
}
