/*
row=4

			D
		C 	C 	C
	B 	B 	B 	B 	B
A 	A 	A 	A 	A 	A 	A
	B 	B 	B 	B 	B
		C 	C 	C
			D

row=3

		C
	B 	B 	B
A 	A 	A 	A 	A
	B 	B 	B
		C
*/

import java.util.*;

class Pattern9{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int col = 0;
		int s = 0;

		for(int i=1; i<rows*2; i++){
			int num = 1;

			if(i<=rows){

				col = i*2-1;
				s = rows-i;
			}else{

				col -= 2;
				s = i-rows;
			}

			for(int sp=1; sp<=s; sp++){

				System.out.print("\t");
		
			}
			for(int j=1; j<=col; j++){

				if(j<=col/2){
					System.out.print((char)(64 + num++) +"\t");
				}else{

					System.out.print((char)(64 + num--) +"\t");
				}
			}
			System.out.println();
		}	
	}
}
