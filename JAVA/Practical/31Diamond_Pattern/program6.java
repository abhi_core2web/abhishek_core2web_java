/*
row = 4

			1
		2	2 	2
	3 	3 	3 	3 	3
4	4 	4 	4 	4 	4 	4
	3 	3 	3 	3	3
		2 	2 	2
			1

row=3

		1
	2 	2 	2
3 	3 	3	3 	3
	2 	2 	2
		1
*/

import java.util.*;

class Pattern6{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int col = 0;
		int s = 0;
		int num = 1;
		for(int i=1; i<rows*2; i++){

			if(i<=rows){

				col = i*2-1;
				s = rows-i;
			}else{

				col -= 2;
				s = i-rows;
			}

			for(int sp=1; sp<=s; sp++){

				System.out.print("\t");
		
			}
			for(int j=1; j<=col; j++){

				System.out.print(num +"\t");
				
			}
			if(i<rows){

				num++;
			}else{
				num--;
			}
			System.out.println();
		}	
	}
}
