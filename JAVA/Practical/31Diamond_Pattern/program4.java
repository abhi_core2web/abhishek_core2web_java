/*
row=3

		1
	1 	2 	1
1 	2 	3 	2 	1
	1 	2 	1	
		1

row=4

			1
		1 	2 	1
	1 	2 	3 	2 	1
1 	2 	3 	4 	3 	2 	1
	1 	2 	3 	2 	1
		1 	2 	1
			1
*/

import java.util.*;

class Pattern4{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int col = 0;
		int s = 0;

		for(int i=1; i<rows*2; i++){
			int num = 1;

			if(i<=rows){

				col = i*2-1;
				s = rows-i;
			}else{

				col -= 2;
				s = i-rows;
			}

			for(int sp=1; sp<=s; sp++){

				System.out.print("\t");
		
			}
			for(int j=1; j<=col; j++){

				if(j<=col/2){
					System.out.print(num++ +"\t");
				}else{

					System.out.print(num-- +"\t");
				}
			}
			System.out.println();
		}	
	}
}
