/*
row = 4

			A
		B 	A 	B
	C 	B 	A 	B 	C
D 	C 	B 	A 	B 	C 	D
	C 	B 	A 	B 	C
		B 	A 	B
			A

row = 3

		A
	B 	A 	B
C 	B 	A 	B 	C
	B 	A 	B
		A
*/

import java.util.*;

class Pattern8{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int col = 0;
		int s = 0;
		int num = 0;

		for(int i=1; i<rows*2; i++){

			if(i<=rows){

				col = i*2-1;
				s = rows-i;
				num = i;
			}else{

				col -= 2;
				s = i-rows;
				num = rows*2-i;
			}

			for(int sp=1; sp<=s; sp++){

				System.out.print("\t");
		
			}
			for(int j=1; j<=col; j++){

				if(j<=col/2){
					System.out.print((char)(64+ num--) +"\t");
				}else{

					System.out.print((char)(64 + num++) +"\t");
				}
			}
			System.out.println();
		}	
	}
}
