/*
5.WAP to take numbers from a user in a 2D array and print the sum of each column in an array.
*/

import java.util.*;

class SumOfCol{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for rows :");
		int rows = sc.nextInt();
		System.out.println("Enter size for columns :");
		int col = sc.nextInt();
		
		int arr[][] = new int[rows][col];
		for(int i=0; i<arr.length; i++){
		

			for(int j=0; j<arr[i].length; j++){
				System.out.println("Enter Element for("+i+","+j+"):");
				arr[i][j]=sc.nextInt();
			}
		}
		System.out.println("Elements are: ");
		for(int i=0; i<arr.length; i++){
	
			for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j]+"  ");
			}
			System.out.println();
		}
		
		int sum[] = new int[col];		
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr[i].length; j++){
				sum[j] += arr[i][j];
			}
		}
		for(int i=0; i<sum.length; i++){

			System.out.println("Sum of "+(i+1)+" column is: "+sum[i]);
		}
	}
}
