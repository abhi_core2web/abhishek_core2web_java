/*
4.WAP to take numbers from a user in a 2D array and print the sum of odd rows in the array.
*/

import java.util.*;

class SumOfOddRows{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size for rows :");
		int rows = sc.nextInt();
		System.out.println("Enter size for columns :");
		int col = sc.nextInt();
		
		int odd = 0;
		int arr[][] = new int[rows][col];
		for(int i=0; i<arr.length; i++){
		
			if((i+1)%2==1){
				odd++;
			}

			for(int j=0; j<arr[i].length; j++){
				System.out.println("Enter Element for("+i+","+j+"):");
				arr[i][j]=sc.nextInt();
			}
		}
		System.out.println("Elements are: ");
		for(int i=0; i<arr.length; i++){
	
			for(int j=0; j<arr[i].length; j++){
				System.out.print(arr[i][j]+"  ");
			}
			System.out.println();
		}
		//int sum[] = new int[odd];		
		for(int i=0; i<arr.length; i++){
			int sum = 0;
			for(int j=0; j<arr[i].length; j++){
				if((1+i)%2==1){
					sum += arr[i][j];
				}else{

					break;
				}
			}
			if((1+i)%2==1){
				System.out.println("sum of "+(i+1)+" rows is "+sum);
			}
		}
	}
}
