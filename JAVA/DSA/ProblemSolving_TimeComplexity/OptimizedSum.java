import java.util.*;
class FindSum{

	static int sum(int num){

		return num * (num+1)/2;
	}

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int ret = sum(num);
		System.out.println(ret);
	}
}
