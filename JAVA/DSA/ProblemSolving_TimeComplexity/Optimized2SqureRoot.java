import java.util.*;
class FindSqureRoot{

	static int findSqureRoot(int num){

		int start = 1;
		int end = num;
		int ans = 0;

		while(start<=end){

			int mid = (start+end)/2;
			int sqr = mid*mid;

			if(mid==num){

				return mid;
			}

			if(sqr>num)
				end = mid-1;
			else{
				start = mid+1;
				ans = mid;
			}
		}
		return ans;
	}

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int ret = findSqureRoot(num);
		System.out.println(ret);
	}
}
