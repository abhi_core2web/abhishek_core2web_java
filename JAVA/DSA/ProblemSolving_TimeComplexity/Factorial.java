import java.util.*;

class FindFactorial{

	static int factorial(int num){
		int rec=0;
		int count = 0;
		for(int i=1; i<=num; i++){

			if(num%i==0)
				count++;

			rec++;
		}
		System.out.println("recursions are: "+rec);
		return count;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		int ret = factorial(n);
		System.out.println(ret);
	}
}
