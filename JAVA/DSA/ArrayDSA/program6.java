// Take input for start and end and print sum b/w them

import java.util.*;

class Sum{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{2,5,3,11,7,9,4};
		int N = arr.length;
	       	int start = sc.nextInt();	
	       	int end = sc.nextInt();	
		int sum = 0;

		if(start < 0 || end > N){

			System.out.println("Please Enter valid inputs");
		}else{

			for(int i=start; i<=end; i++){

				sum += arr[i];
			}
			System.out.println(sum);
		}


	}
}
