// Take input for start and end and print sum b/w them 

import java.util.*;

class Sum{

	static int sum(int arr[], int s, int e){

		int sum = 0;
		int N = arr.length;

		for(int i=s; i<=e; i++){

			sum += arr[i];
			System.out.println(i+" = "+sum);
		}
		return sum;
	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = arr.length;
		int k = 3;
		
		for(int i=1; i<=k; i++){

		       	int s = sc.nextInt();	
		       	int e = sc.nextInt();	

			if(s < 0 || e > N-1){

				System.out.println("Please Enter valid inputs");
			}else{

				int ret = sum(arr,s,e);
				System.out.println("Sum b/w "+s+" and "+e+" is "+ret);
			
			}

		}
	}
}
