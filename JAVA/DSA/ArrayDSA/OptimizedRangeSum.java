import java.util.*;

class RangeSum{

	public static void main(String[] args){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = 10;
		int Q = 3;

		int psArr[] = new int[N];

		psArr[0] = arr[0];

		for(int i=1; i<N; i++){

			psArr[i] = psArr[i-1] + arr[i];		//[-3,3,5,9,14,16,24,15,18,19]
		}
		
		Scanner sc = new Scanner(System.in);

		int sum = 0;
		for(int i=1; i<=Q; i++){
			
			int s = sc.nextInt();
			int e = sc.nextInt();
	
			if(s==0)
				sum = psArr[e];
			else
				sum = psArr[e] - psArr[s-1];

			System.out.println(sum);
		}


	}
}
