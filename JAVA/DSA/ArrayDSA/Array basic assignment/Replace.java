/*
 * 5] Replace all 0's with 5

You are given an integer N. You need to convert all zeros of N to 5.
Example 1:
Input:

N = 1004
Output: 1554
Explanation: There are two zeroes in 1004
on replacing all zeroes with "5", the new
number will be "1554".
 */

 import java.util.Scanner;
class Replace {
    
    static int replaceZero(int n){

	    int reverseNum = 0;

	    while(n>0){

		    int rem = n % 10;
		    reverseNum = reverseNum * 10 + rem;
		    n /= 10;
	    }

	    int updatedNum = 0;
	    while(reverseNum > 0){
		int rem = reverseNum%10;
	        
		if(rem == 0){
	            updatedNum = updatedNum*10 + 5;
	        }else{
	            updatedNum = updatedNum*10 + rem;
	        }
	        reverseNum= reverseNum/10;
	    }
	return updatedNum;        
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Number: ");
        int n = sc.nextInt();

        int replace = replaceZero(n);
        System.out.println("Replace with Zero:"+ replace);

    }
}
