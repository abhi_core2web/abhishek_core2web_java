/*
 * You are given an array Arr of size N. Find the sum of distinct elements in an array.
Example 1:
Input:
N = 5
Arr[] = {1, 2, 3, 4, 5}
Output: 15
Explanation: Distinct elements are 1, 2, 3
4, 5. So the sum is 15.
Example 2:
Input:
N = 5
Arr[] = {5, 5, 5, 5, 5}
Output: 5
Explanation: Only Distinct element is 5.
So the sum is 5.

Expected Time Complexity: O(N*logN)
Expected Auxiliary Space: O(N*logN)
Constraints:
1 ≤ N ≤ 10^7
0 ≤ A[i] ≤ 10^4
 */

class DistinctSum {

    //[1,2,1,4,5]
    static int findSumOfDistinctElements(int arr[]){
        int sum=0;
    
        //sort the array
        // Arrays.sort(arr);
        for(int i=0; i<arr.length-1; i++) {
            for(int j=0; j<arr.length-1-i; j++) {
                if(arr[j]>arr[j+1]) {
                    arr[j] = arr[j] + arr[j+1];
                    arr[j+1] = arr[j] - arr[j+1];
                    arr[j] = arr[j] - arr[j+1];
                }
            }
        }


        for(int i=0; i<arr.length-1; i++) {
        
            if(arr[i] == arr[i+1]) {
                System.out.println("Duplicate element: "+arr[i]);
                continue;
            }else {
                sum = sum + arr[i];
            }
        }
        //This manage the last element of the array
        sum = sum + arr[arr.length-1];
        return sum;
    }
    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5};
        int sum = findSumOfDistinctElements(arr);
        System.out.println(sum);
    }
}