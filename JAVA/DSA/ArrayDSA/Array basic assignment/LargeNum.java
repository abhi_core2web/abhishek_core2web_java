/*
 * 7] Form largest number from digits

Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
of the array such that after combining all the elements of the array, the number
formed is maximum.
Example 1:
Input:
N = 5
A[] = {9, 0, 1, 3, 0}
Output:
93100
Explanation:
Largest number is 93100 which can be formed from array digits.
*/

class LargeForm {

	static void largeNumber(int arr[], int n) {

		for (int i = 0; i < n-1; i++) {

			for(int j=0; j<n-i-1; j++) {
				if(arr[j] < arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		for (int i = 0; i < n; i++) {
			System.out.print(arr[i]);
		}
	}

	public static void main(String[] args) {

		int arr[] = new int[] { 9, 8, 1, 10, 0 };
		int n = arr.length;

		largeNumber(arr, n);
	}
}
