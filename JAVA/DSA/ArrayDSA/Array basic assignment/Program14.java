/*
 * 14] Maximum repeating number
    Given an array Arr of size N, the array contains numbers in range from 0 to K-l
    where K is a positive integer and K <= N. Find the maximum repeating number in
    this array. If there are two or more maximum repeating numbers return the element
    having least value.
    Example 1:
    Input:
    Arr[] = {2, 2, 1, 2}
    Output: 2
    Explanation: 2 is the most frequent element.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(K)
    Constraints:

 */

 import java.util.*;

public class Program14 {

    static void maxRepeatingNumber(int arr[]) {
                int n = arr.length;
        for(int i=0; i<n; i++) {
            arr[arr[i] % n] += n;
            System.out.print(arr[i] +"\t");
        }
        System.out.println();
        
        
        int max = arr[0], result = 0;
        for(int i=1; i<n; i++) {
            if(arr[i] > max) {
                max = arr[i];
                result = i;
            }
        }
        System.out.println("Maximum repeating number is: "+result);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array ");
        int n = sc.nextInt();

        System.out.println("Enter k that is <= "+ n +" :");
        int k = sc.nextInt();
        
        int arr[] = new int[n];
        System.out.println("Enter Element of array: ");

        int i = 0;
        while(i<arr.length) {
            int x = sc.nextInt();
            if(x > k) {
                System.out.println("Please enter in between 0 to "+k);
            }else{
                arr[i] = x;
                i++;
            }
        }
        
        for(int x=0; x<arr.length; x++) {
            System.out.print(arr[x] +"\t");
        }
        System.out.println();
        maxRepeatingNumber(arr);


    }
}
