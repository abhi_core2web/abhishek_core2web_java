/*
 * Find common elements in three sorted arrays

Given three Sorted arrays in non-decreasing order, print all common elements in
these arrays.
Examples:
Input:
ar1[] = {1, 5, 10, 20, 40, 80}
ar2[] = {6, 7, 20, 80, 100}
ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
Output: 20, 80
Input:
ar1[] = {1, 5, 5}
ar2[] = {3, 4, 5, 5, 10}
ar3[] = {5, 5, 10, 20}
Output: 5, 5
 */

class commonFromThree {

    static void findCommon(int arr1[],int arr2[], int arr3[]) {
        int i=0, j=0, k=0, count=0;
        while(i<arr1.length && j<arr2.length && k<arr3.length) {
            if(arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
                System.out.println(arr1[i]);
                i++;
                j++;
                k++;
                count++;
            }else if(arr1[i] < arr2[j]) {
                i++;
                count++;
            }else if(arr2[j] < arr3[k]) {
                j++;
                count++;
            }else {
                k++;
                count++;
            }
        }
        System.out.println("Total common elements are: "+count);
    }
    public static void main(String[] args) {
        int arr1[] ={1, 5, 10, 20, 40, 80};
        int arr2[] = {6, 7, 20, 80, 100};
        int arr3[] = {3, 4, 15, 20, 30, 70, 80, 120};
        findCommon(arr1, arr2, arr3);
    }
}