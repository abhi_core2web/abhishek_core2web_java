// 4] Product of array elements

// This is a functional problem. Your task is to return the product of array elements
// under a given modulo.
// The modulo operation finds the remainder after the division of one number by
// another. For example, K(mod(m))=K%m= remainder obtained when K is divided
// by m
// Example 1:
// Input:
// 1
// 4
// 1 2 3 4
// Output:
// 24

import java.util.Scanner;

class ProductArray {
    
    static int product(int arr[], int n, int mod){

        int product = 1;

        for(int i=0; i<n; i++){
            product = product * arr[i];
        }
        return product % mod;
    }

    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,4};
        int n = arr.length;
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the modulo: ");
        int modulo = sc.nextInt();

        int product = product(arr, n, modulo);
        System.out.println("Product of array elements under modulo " + modulo + " is: " + product);
    }
}
