/*
 * Check if pair with given Sum exists in Array (Two Sum)

Given an array A[] of n numbers and another number x, the task is to check
whether or not there exist two elements in A[] whose sum is exactly x.
Examples:
Input: arr[] = {0, -1, 2, -3, 1}, x= -2
Output: Yes
Explanation: If we calculate the sum of the output,1 + (-3) = -2
Input: arr[] = {1, -2, 1, 0, 5}, x = 0
Output: No
 */

import java.util.*;

import javafx.scene.control.TableColumn.SortType;

class PairWithSum {

    public static void main(String[] args) {
        // Scanner sc = new Scanner(System.in);
        // System.out.println("Enter the size of array: ");
        // int n = sc.nextInt();
        int arr[] = {0, -1, 2, -3, 1};
        int x = -44;
        int sum = 0;

        for(int i=0; i<arr.length; i++) {
            for(int j=i+1; j<arr.length;j++){
                sum = arr[i] + arr[j];
                if (sum==x) {
                    System.out.println("yes");
                }
            }
        }
        if(sum!=x)
            System.out.println("No");
    }
}