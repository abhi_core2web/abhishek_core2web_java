/*
 * First element to occur k times

Given an array of N integers. Find the first element that occurs at least K number
of times.
Example 1:
Input :
N = 7, K = 2
A[] = {1, 7, 4, 3, 4, 8, 7}
Output :
4
Explanation:
Both 7 and 4 occur 2 times.
But 4 is first that occurs 2 times
As at index = 4, 4 has occurred
at least 2 times whereas at index = 6,
7 has occurred at least 2 times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^4
1 <= K <= 100
1<= A[i] <= 200
 */

import java.util.*;

class FindOccurKTimes {

    public static void main(String[] args) {
        // Scanner sc = new Scanner(System.in);
        // System.out.println("Enter the size of array: ");
        // int n = sc.nextInt();
        // System.out.println("Enter the value of k: ");
        int k = 2;
        // System.out.println("Enter the elements of array: ");
        // for(int i=0; i<arr.length; i++) {
        //     arr[i] = sc.nextInt();
        // }
        int arr[] = {1, 7, 4, 8, 4, 8, 7};
        Arrays.sort(arr);
      
    }
}
