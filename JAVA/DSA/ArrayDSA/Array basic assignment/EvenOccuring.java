/*
 *8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
 * 
 * */

class EvenOccuring {
	public static void main(String[] args) {
		int arr[] = { 9, 12, 23, 10, 12, 12, 15, 23, 14, 12, 15 };
		int n = arr.length;
		int count = 1;
		
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n- 1; j++) {

				if (arr[j] > arr[j + 1]) {
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}

		for (int i = 0; i < n; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		for (int i = 0; i < n-1; i++) {
			if(arr[i] == arr[i+1]) {
				count++;
			}else {
				if(count % 2 == 0) {
					System.out.print(arr[i] + " ");
				}
				count = 1;
			}
		}
		if(count % 2 == 0) {
			System.out.print(arr[n-1] + " ");
		}
	}
}
