/*
 * 6] Elements in the Range

Given an array arr[] containing positive elements. A and B are two numbers
defining a range. The task is to check if the array contains all elements in the given
range.
Example 1:
Input: N = 7, A = 2, B = 5
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: Yes
Explanation: It has elements between range 2-5 i.e 2,3,4,5
 */

 class ElementInRange{

	 static int rangeCheck(int arr[],int n,int a,int b){

		 int cnt = 0;
		 for(int i=0; i<n; i++){

			 if(arr[i]>=a && arr[i]<=b){

				 cnt++;
			 }
		 }
		 return cnt;
	 }
	public static void main(String[] args){

		int arr[] = new int[]{1,4,5,2,7,8,3};
		int n = arr.length;
		int a = 2;
		int b = 5;

		int diff = (b-a)+1;

		int cnt = rangeCheck(arr,n,a,b);
		if(diff==cnt)

			System.out.println("Yes");
		else
			System.out.println("No");

	}	
 }
