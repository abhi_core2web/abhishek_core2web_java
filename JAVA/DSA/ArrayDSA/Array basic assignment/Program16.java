/*
 * 16] Last index of One
Given a string S consisting only '0's and '1's, find the last index of the '1' present in
it.
Example 1:
Input:
S = 00001
Output:
4
Explanation:
Last index of 1 in the given string is 4.

Example 2:
Input:
0
Output:
-1
Explanation:
Since, 1 is not present, so output is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= |S| <= 10^6
S = {0,1}
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.*;

class FindLastIndexOf1 {

    static int findLastIndex(String s) {
        int n = s.length();
        int index = 0;

        for(int i=0; i<n; i++) {
            if(s.charAt(i) == '1' && i==n-1) {
                index = i;
            }else{
                index = -1;
            }
        }
        return index;
    }
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please Enter the String but only give 0 and 1 string: ");
        String s = br.readLine();

        int index = findLastIndex(s);
        System.out.println(index);
    }
}