/*
 * Product of maximum in first array and minimum in second

Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array.
Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
 */
import java.util.*;
class ProductOfMaxMin {
    
    static int productofMaxMin (int arr1[], int arr2[]) {

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        for(int i=0; i<arr1.length; i++){

            if(arr1[i]>max)
                max = arr1[i];
        }

        for(int i=0; i<arr2.length; i++){

            if(arr2[i]<min) 
                min = arr2[i];
        }
        return max * min;
    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the first array: ");
        int n1 = sc.nextInt();
        System.out.println("Enter the size of the second array: ");
        int n2 = sc.nextInt();

        int arr1[] = new int[n1];
        int arr2[] = new int[n2];

        System.out.println("Enter the elements of the first array: ");
        for(int i = 0; i < n1; i++) {
            arr1[i] = sc.nextInt();
        }
        System.out.println("Enter the elements of the second array: ");
        for(int i = 0; i < n2; i++) {
            arr2[i] = sc.nextInt();
        }
        
        int product = productofMaxMin(arr1, arr2);
        System.out.println("The product of the maximum element of the first array and the minimum element of the second array is: " + product);
    }
}
