/*
Find minimum and maximum element in an array

Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
Example 1:
Input:
N = 6
A[] = {3, 2, 1, 56, 10000, 167}
Output: 1 10000


Constraints:
1 <= N <= 10^5
1 <= Ai <=10^12
 */


class MinMax {
    
    static void min(int arr[]){

        int min = Integer.MAX_VALUE;
        for(int i=0; i<arr.length; i++){
            if(arr[i] < min)
                min = arr[i];
        }
        System.out.println("Minimum value: "+min);
    }
    static void max(int arr[]){

        int max = Integer.MIN_VALUE;
        for(int i=0; i<arr.length; i++){
            if(arr[i] > max)
                max = arr[i];
        }
        System.out.println("Maximum value: "+max);

    }

    public static void main(String[] args){

        int arr[] = new int[]{3,2,56,2,3,6,4,7,2};
        max(arr);
        min(arr);
    }
}
