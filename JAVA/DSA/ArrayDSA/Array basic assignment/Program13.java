// 13] Find unique element

// Given an array of size n which contains all elements occurring in multiples of K,
// except one element which doesn't occur in multiple of K. Find that unique element.
// Expected Time Complexity: O(N. Log(A[i]) )
// Expected Auxiliary Space: O( Log(A[i]) )
// Constraints:
// 3<= N<=2*10^5
// 2<= K<=2*10^5
// 1<= A[i]<=10^9\

import java.util.Scanner;
public class Program13 {

    static int findUniqueElement(int arr[], int k){

        int uniqueNum = 0;
        int n = arr.length;
        int count[] = new int[32];

        for(int i = 0; i<32; i++){
            for(int j = 0; j<n; j++){
                if((arr[j] & (1<<i)) != 0){
                    count[i]++;
                }
            }
        }

        for(int i = 0; i<32; i++){
            count[i] = count[i] % k;
        }

        for(int i = 0; i<32; i++){
            uniqueNum += (count[i] * (1<<i));
        }

        
        return uniqueNum;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array: ");
        int x = sc.nextInt();

        System.out.println("Enter the value of K: ");
        int k = sc.nextInt();
        System.out.println("Enter the array elements: ");   
        int arr[] = new int[x];

        for(int i = 0; i<x; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("The unique element is: " + findUniqueElement(arr, k));

    }
}