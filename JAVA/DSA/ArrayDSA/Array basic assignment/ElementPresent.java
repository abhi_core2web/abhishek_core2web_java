/*
Given an integer array and another integer element. The task is to find if the given
element is present in the array or not.
Example 1:
Input:
n = 4
arr[] = {1,2,3,4}
x = 3
Output: 2

Explanation: There is one test case with an array as {1, 2, 3 4} and an
element to be searched as 3. Since 3 is present at index 2, output is 2.
*/

import java.util.Scanner;

class Element {

	static void find(int arr[], int n){

		for(int i=0; i<arr.length; i++){

			if(arr[i]==n){
				System.out.println(i);
				break;
			}	
		}
	}
	public static void main(String[] args){
	
		int arr[] = new int[]{1,2,3,4};
		
		Scanner sc  = new Scanner(System.in);
		System.out.println("Enter Element to find in the array: ");
		int x = sc.nextInt();
		find(arr, x);
	}
}
