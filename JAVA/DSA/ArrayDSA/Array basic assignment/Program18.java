/*
 * Find Subarray with given sum | Set 1 (Non-negative
Numbers)
Given an array arr[] of non-negative integers and an integer sum, find a subarray
that adds to a given sum.
Note: There may be more than one subarray with sum as the given sum, print first
such subarray.
Examples:
Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Output: Sum found between indexes 2 and 4
Explanation: Sum of elements between indices 2 and 4 is 20 + 3 + 10 = 33
Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Output: Sum found between indexes 1 and 4
Explanation: Sum of elements between indices 1 and 4 is 4 + 0 + 0 + 3 = 7
Input: arr[] = {1, 4}, sum = 0
Output: No subarray found
Explanation: There is no subarray with 0 sum
 */

import java.util.Scanner;

class SubarrayWithSum {

    //Brute force approach
    static void subarrayWithSum(int arr[], int sum) {

        int startIndex = 0;
        int endINdex = 0;
        

        for(int i=0; i<arr.length; i++) {
            int tmpSum = arr[i];
            startIndex = i;
            for(int j=i+1; j<arr.length; j++) {
                if(tmpSum == sum) {
                    endINdex = j;
                    System.out.println(arr[startIndex]);
                    break;
                }else{
                    tmpSum += arr[j];
                    if(tmpSum == sum || tmpSum > sum) {
                        endINdex = j;
                        break;
                    }
                }
            }
            if(tmpSum == sum) 
                break;
        }
        for(int i=startIndex+1; i<=endINdex; i++) {
            System.out.println(arr[i]);
        }
    }   
    
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array");
        int n = sc.nextInt();
        int arr[] = new int[n];
        int i = 0;
        System.out.println("Enter the elements of array: ");
        while(i<arr.length) {

            int num = sc.nextInt();
            if(num >= 0) {
                arr[i] = num;
                i++;
            }else{
                System.out.println("Please enter non-negative number");
                continue;
            }
        }
        int sum = 23;
        subarrayWithSum(arr, sum);
    }
}