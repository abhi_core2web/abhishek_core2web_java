/*
 Largest Element in Array

Given an array A[] of size n. The task is to find the largest element in it.
Example 1:
Input:
n = 5
A[] = {1, 8, 7, 56, 90}
Output: 90
Explanation:
The largest element of a given array is 90.

Constraints:
1 <= n<= 10^3
0 <= A[i] <= 10^3
 */

class Large {
    
    void largest(int arr[]){
        
        int max = Integer.MIN_VALUE;
        for(int i=0; i<arr.length; i++){
            if(arr[i] > max)
                max = arr[i];
        }
        System.out.println("Largest Element in the array: "+max);
    }
    public static void main(String[] args) {
        
        Large obj = new Large();
        int arr[] = new int[]{1, 8, 7, 56, 90};
        obj.largest(arr);
    }
}
