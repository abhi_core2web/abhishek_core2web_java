class HelloException{
    HelloException(String str){
            super(str);
    }
}
class Demo{

    public static void main(String[] args){

            String str ="Hello";
            if(str == "Hello")
                    throw new HelloException("Hi");
    }
}
// what is missing in this code?