import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CipherApp {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose an option:");
        System.out.println("1. Caesar Cipher");
        System.out.println("2. Substitution Cipher");
        int choice = scanner.nextInt();
        scanner.nextLine();  // consume newline

        System.out.println("Enter the text:");
        String text = scanner.nextLine();

        String result = "";
        switch (choice) {
            case 1:
                System.out.println("Enter the shift:");
                int shift = scanner.nextInt();
                result = caesarCipher(text, shift);
                System.out.println("Encrypted text: " + result);
                System.out.println("Decrypted text: " + caesarCipher(result, 26 - shift));
                break;
            case 2:
                System.out.println("Enter the substitution alphabet:");
                String substitutionAlphabet = scanner.nextLine();
                result = substitutionCipher(text, substitutionAlphabet);
                System.out.println("Encrypted text: " + result);
                System.out.println("Decrypted text: " + substitutionDecipher(result, substitutionAlphabet));
                break;
            default:
                System.out.println("Invalid choice");
                break;
        }
        
        scanner.close();
    }

    public static String caesarCipher(String text, int shift) {
        StringBuilder result = new StringBuilder();
        shift = shift % 26 + 26;
        for (char i : text.toCharArray()) {
            if (Character.isLetter(i)) {
                if (Character.isUpperCase(i)) {
                    result.append((char) ('A' + (i - 'A' + shift) % 26 ));
                } else {
                    result.append((char) ('a' + (i - 'a' + shift) % 26 ));
                }
            } else {
                result.append(i);
            }
        }
        return result.toString();
    }

    public static String substitutionCipher(String text, String key) {
        StringBuilder result = new StringBuilder();
        for (char i : text.toCharArray()) {
            if (Character.isLetter(i)) {
                if (Character.isUpperCase(i)) {
                    result.append(Character.toUpperCase(key.charAt(i - 'A')));
                } else {
                    result.append(Character.toLowerCase(key.charAt(i - 'a')));
                }
            } else {
                result.append(i);
            }
        }
        return result.toString();
    }

    public static String substitutionDecipher(String text, String key) {
        StringBuilder result = new StringBuilder();
        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < key.length(); i++) {
            map.put(Character.toUpperCase(key.charAt(i)), (char) ('A' + i));
            map.put(Character.toLowerCase(key.charAt(i)), (char) ('a' + i));
        }
        for (char i : text.toCharArray()) {
            if (Character.isLetter(i)) {
                result.append(map.get(i));
            } else {
                result.append(i);
            }
        }
        return result.toString();
    }
}
