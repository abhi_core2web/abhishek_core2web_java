import java.util.*;

class HashSetOrder {

    private static final int BUCKET_SIZE = 16; // Example bucket size

    private static class StringBucketInfo {
        String str;
        int bucketIndex;

        StringBucketInfo(String str, int bucketIndex) {
            this.str = str;
            this.bucketIndex = bucketIndex;
        }

        @Override
        public String toString() {
            return str + " (Bucket: " + bucketIndex + ")";
        }
    }

    private static void findBucketOrder(HashSet<String> names) {
        // Create a map to hold bucket indices
        Map<Integer, List<String>> bucketMap = new HashMap<>();

        for (String name : names) {
            // Get the built-in hash code
            int hashCode = name.hashCode();
            
            // Calculate bucket index using the bucket size
            int bucketIndex = (hashCode & (BUCKET_SIZE - 1));

            // Add the string to the corresponding bucket in the map
            bucketMap.computeIfAbsent(bucketIndex, k -> new ArrayList<>()).add(name);
        }

        // Print the results
        System.out.println("HashSet order based on bucket indices:");
        for (Map.Entry<Integer, List<String>> entry : bucketMap.entrySet()) {
            int bucketIndex = entry.getKey();
            List<String> bucketContents = entry.getValue();
            System.out.println("Bucket " + bucketIndex + ": " + bucketContents);
        }
    }
    
    public static void main(String[] args) {
        HashSet<String> names = new HashSet<>();
        names.add("Cpp");
        names.add("React");
        names.add("Flutter");
        names.add("SpringBoot");
        names.add("Java");
        names.add("Python");
        names.add("Swift");

        System.out.println("HashSet: " + names);
        findBucketOrder(names);
    }
}
