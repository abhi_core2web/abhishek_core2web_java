import java.util.*;

class PriorityQueueDemo {
    
    public static void main(String[] args) {
        PriorityQueue pq = new PriorityQueue();
        pq.add(10);
        pq.add(30);
        pq.add(50);
        pq.add(40);
        pq.add(20);
        System.out.println(pq);

    }
}
// कोड मध्ये ऑफर या मेथड च्या आत shifUp ला call जातो with @argument size and Element 
// shitUp च्या आत त्याच्या आत्ता else मध्ये जाऊन siftUpComparable @param  size and Element method ला  call केला जातो
// या मेथड च्या आत elment ला comparable मध्ये करून तो compare केला जातो त्याच्या parent index ला असलेल्या element शी 
// जर comparision newELement < parentElement असेल तर तो shiftUp  करतो आणि elemnt store karto