import java.util.concurrent.*;

class Producer implements Runnable {
    private BlockingQueue bq = null;
    public Producer(BlockingQueue bq) {
        this.bq = bq;
    }
    public void run() {
       for(int i=0; i<10; i++) {
          try {
            bq.put(i);
            System.out.println("Producer produced: "+i);
          }catch(InterruptedException e) {
            e.printStackTrace();
          }
          try {
            Thread.sleep(2000);
          }catch(InterruptedException e) {
            e.printStackTrace();
          }
       }
    }
}

class Consumer implements Runnable {
    private BlockingQueue bq = null;
    public Consumer(BlockingQueue bq) {
        this.bq = bq;
    }
    public void run() {
       for(int i=0; i<10; i++) {
          try {
            bq.take();
            System.out.println("Consumer consumed: "+i);
          }catch(InterruptedException e) {
            e.printStackTrace();
          }
          try {
            Thread.sleep(1000);
          }catch(InterruptedException e) {
            e.printStackTrace();
          }
       }
    }
}

class ConsumerProducer {
 
    public static void main(String[] args) {
        
        BlockingQueue bq = new ArrayBlockingQueue(10);
        Producer p = new Producer(bq);
        Consumer c = new Consumer(bq);
        Thread t1 = new Thread(p);
        Thread t2 = new Thread(c);
        t1.start();
        t2.start();


    }
}
