
/*
 * Given two words (start and end), and a dictionary, find the length of shortest transformation sequence from start to end, such that:
 * Only one letter can be changed at a time
 * Each intermediate word must exist in the dictionary
 */

 import java.util.*;

public class WordLadder {
    public static void main(String[] args) {
        String start = "hit";
        String end = "cog";
        String[] dict = {"hot", "dot", "dog", "lot", "log"};

        System.out.println(ladderLength(start, end, dict));
    }

    private static int ladderLength(String start, String end, String[] dict) {
        Set<String> dictSet = new HashSet<>(Arrays.asList(dict));
        
        // Add the end word to the dictionary if it's not present
        if (!dictSet.contains(end)) {
            dictSet.add(end);
        }

        LinkedList<String> wordQueue = new LinkedList<>();
        LinkedList<Integer> distanceQueue = new LinkedList<>();
        wordQueue.add(start);
        distanceQueue.add(1);

        while (!wordQueue.isEmpty()) {
            String currWord = wordQueue.poll();
            Integer currDistance = distanceQueue.poll();

            if (currWord.equals(end)) {
                return currDistance;
            }

            for (int i = 0; i < currWord.length(); i++) {
                char[] currCharArr = currWord.toCharArray();
                for (char c = 'a'; c <= 'z'; c++) {
                    if (currCharArr[i] == c) continue; // Skip if the character is the same
                    currCharArr[i] = c;
                    String newWord = new String(currCharArr);

                    if (dictSet.contains(newWord)) {
                        wordQueue.add(newWord);
                        distanceQueue.add(currDistance + 1);
                        dictSet.remove(newWord); // Remove to prevent revisiting-
                    }
                }
            }
        }

        return 0; // No transformation sequence found
    }
}
