
class ThreadDemo extends Thread{

    ThreadDemo(ThreadGroup tg,String name){

            super(tg,name);
    }
    public void run(){

            getThreadGroup().setMaxPriority(1);
            System.out.println("Thread Group : "+getThreadGroup().getMaxPriority());
            System.out.println("Thread : "+getPriority());
    }
}

class Client {

    public static void main(String[] args){

            ThreadGroup tg = new ThreadGroup("C2W");
            ThreadDemo td1 = new ThreadDemo(tg,"JAVA");
            td1.start();
    }
}