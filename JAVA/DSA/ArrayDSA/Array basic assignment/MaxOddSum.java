// /*10] Max Odd Sum

// Given an array of integers, check whether there is a subsequence with odd sum and
// if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
// print -1.
// Example 1:
// Input:
// N=4
// arr[] = {4, -3, 3, -5}
// Output: 7
// Explanation:
// The subsequence with maximum odd
// sum is 4 + 3 = 7
//  *
//  * /

public class MaxOddSum {
    public static void main(String[] args) {
        int[] arr = {4, -3, 3, -5,2}; // Example input
        System.out.println(findMaxOddSum(arr));
    }

    public static int findMaxOddSum(int[] arr) {
        int maxSum = Integer.MIN_VALUE;
        boolean foundOdd = false;

        // Iterate through all possible subsequences
        for (int i = 0; i < (1 << arr.length); i++) {
            int sum = 0;
            for (int j = 0; j < arr.length; j++) {
                // Check if jth element is included in current subsequence
                if ((i & (1 << j)) != 0) {
                    sum += arr[j];
                }
            }
            // Check if the sum is odd and greater than maxSum
            if (sum % 2 != 0) {
                foundOdd = true;
                maxSum = Math.max(maxSum, sum);
            }
        }

        // Return -1 if no odd sum subsequence is found
        return foundOdd ? maxSum : -1;
    }
}