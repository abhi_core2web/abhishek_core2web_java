/*
 * 9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
 */

import java.util.Scanner;

class RemoveElement {

	static void removeElement(int arr[], int n, int index){

		int arr1[] = new int[n-1];
		for(int i=0, k=0; i<n; i++){

			if(i==index)
				continue;
			arr1[k++] = arr[i];
		}

		for(int i=0; i<n-1; i++){

			System.out.print(arr1[i]+"\t");
		}
	}

	public static void main(String[] args){

		int n = 5;
		int arr[] = new int[n];
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the element:");
		for(int i=0; i<n; i++){

			arr[i] = sc.nextInt(); 
		}

		System.out.println("Enter index to remve element : ");
		int index = sc.nextInt();

		if(index>=n || index<0)

			System.out.println("Index out of range");
		else
			removeElement(arr,n,index);
	}
}
