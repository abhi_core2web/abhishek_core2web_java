/*
 * 12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N = 4 , X = 3
arr[] = { 1, 3, 3, 4 }
Output:
1 2
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2.
 */

 import java.util.Scanner;

 class FirstLastOccurence {

    static void firstLastOccurence (int arr[], int x, int n){

        int first = -1;
        int last = -1;

        for(int i=0; i<n; i++){

            if(arr[i] == x ) {
                first = i;
                break;
            }
        }

        for(int i= first+1; i<n; i++){
            if(arr[i] == x){
                last = i;
            }
        }

        if(first == -1){
            System.out.println("-1");
        }else{
            System.out.println(first + " " + last);
        }
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
        System.out.println("Enter the elements of the array: ");
        for(int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        System.out.println("Enter the element to find the first and last occurence: ");
        int x = sc.nextInt();

        firstLastOccurence(arr, x, n);

    }
 }
 