/*
 *    Que: Given an integer array of size N
 *    	   Build ana array LeftMax of Size N.
 *    	   Leftmax of i contains maximum for the index 0 to the index i
 */

class LeftMax{

	public static void main(String[] args){
		
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = arr.length;

		int leftMax[] = new int[N];
		int max = Integer.MIN_VALUE;

		leftMax[0] = arr[0];

		for(int i=1; i<N; i++){

			if(leftMax[i-1] < arr[i])
				leftMax[i] = arr[i];
			else
				leftMax[i] = leftMax[i-1];		//{1,2,43,4,5}
		}

		System.out.println("Orignal array: ");
		for(int i=0; i<N; i++){

			System.out.print(arr[i]+"\t");
		}
		
	
		System.out.println("\nLeftMax array: ");
		for(int i=0; i<N; i++){

			System.out.print(leftMax[i]+"\t");
		}


	}
}
