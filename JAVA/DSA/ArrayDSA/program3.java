/*
 Que : Given an array of size N, returns the count of pairs(i,j) with arr[i]+ arr[j]=k

arr:[2,5,2,1,-3,7,8,15,6,13];
N = 10;
k = 10

Note i != j;

*/
import java.util.Scanner;
class CountPairs{

	static int countPairsWithSum(int arr[], int N, int k){
		int count = 0;
		for(int i=0; i<N; i++){

			for(int j=0; j<N; j++){
			
				if(i!=j){
					if(arr[i]+arr[j]==k)
					count++;
				}else
					continue;
			}
		}
		return count;
	}
	
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array ");
		int N = sc.nextInt();

		int arr[] = new int[N];
		System.out.println("Enter the array");
		for(int i=0; i<N; i++){

			arr[i] = sc.nextInt();
		}
		System.out.println("Enter the K vlaue ");
		int k = sc.nextInt();
				
		int count = countPairsWithSum(arr,N,k);
		System.out.println("Count is "+count);
	}
}

//Time Complexity = O(N*N)
//Space Complexity = O(1)
