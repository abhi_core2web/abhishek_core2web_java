/*
 *    Que: Given an integer array of size N
 *    	   Build ana array RightMax of Size N.
 *    	   Rightmax of i contains maximum for the index i to the index n-1
 */

class RightMax{

	public static void main(String[] args){
		
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = arr.length;

		int rightMax[] = new int[N];

		rightMax[N-1] = arr[N-1];

		for(int i=N-2; i>=0; i--){

			if(rightMax[i+1] < arr[i])
				rightMax[i] = arr[i];
			else
				rightMax[i] = rightMax[i+1];		//{1,2,43,4,5}
		}

		System.out.println("Orignal array: ");
		for(int i=0; i<N; i++){

			System.out.print(arr[i]+"\t");
		}
		
	
		System.out.println("\nRightMax array: ");
		for(int i=0; i<N; i++){

			System.out.print(rightMax[i]+"\t");
		}


	}
}
