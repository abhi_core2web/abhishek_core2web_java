/*
 Que : Given an array of size N, Count the number of elements having at least one elemnt greater than itself

arr:[2,5,1,4,8,0,8,1,3,8];
N = 10;

*/
import java.util.Scanner;
class CountsElement{

	
	static int Count(int arr[], int N){

		int count = 0;
		int max = Integer.MIN_VALUE;
		for(int i=0; i<N; i++){

			if(max < arr[i]){
				max = arr[i];
			}
		}
		for(int i=0; i<N; i++){

			if(max==arr[i])
				count++;
		}
		return N - count;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array ");
		int N = sc.nextInt();

		int arr[] = new int[N];
		System.out.println("Enter the array");
		for(int i=0; i<N; i++){

			arr[i] = sc.nextInt();
		}
		int count = Count(arr,N);
		System.out.println("Count is "+count);
	}
}

//Time Complexity = O(N)
//Space Complexity = O(1)
