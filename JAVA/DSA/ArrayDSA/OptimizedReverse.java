// Que: reverse the array with the T.C. = O(N) and S.C. =O(1)

class Reverse{

	public static void main(String[] args){

		int arr[] = new int[]{1,2,4,5,3,6};
		int N = arr.length;

		for(int i=0; i<N; i++){

			arr[i] = arr[i]+arr[N-(1+i)];
			arr[N-(1+i)] = arr[i] - arr[N-(1+i)];
			arr[i] = arr[i] - arr[N-(1+i)];
		}

		for(int i=0; i<N; i++){

			System.out.print(arr[i]+"\t");
		}
	}
}
