class CharPair {    
    static int pair(char arr[]){

        int cnt = 0;
        for(int i=0; i< arr.length; i++){   
            if(arr[i]=='a'){
                for(int j=i+1; j<arr.length; j++){
                    if(arr[j]=='g'){
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }
    public static void main(String[] args) {
        char arr[] = new char[]{'a','b','e','g','a','g'};
        int cnt = pair(arr);
        System.out.println(cnt);
    }
}
