//Rotating array K times and return same array
//

import java.io.*;

class ArrayRotate{

	static int[] rotation(int arr[], int N, int K){
		
		int arr1[] =new int[N];
		int x = 0;
		for(int i = K; i<N; i++){
		
			arr1[x] = arr[i];
			x++;
		}
		for(int i=0; i<K; i++){
			
			arr1[x]= arr[i];
			x++;
		}

		for(int i=0; i<N; i++){

			arr[i] = arr1[i];
		}
		return arr1;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int arr[] = new int[]{1,2,3,4};
		System.out.println("Enter how many time to rotate.");
		int k = Integer.parseInt(br.readLine());

		int N = arr.length;
	
		arr = rotation(arr,N,k);
		
		for(int i=0; i<N; i++){

			System.out.print(arr[i]+"\t");
		}

	}
}
