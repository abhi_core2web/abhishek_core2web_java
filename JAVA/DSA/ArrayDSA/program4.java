// Que: reverse the array with the T.C. and S.C. =O(N)

class Reverse{

	public static void main(String[] args){

		int arr[] = new int[]{1,2,4,5,3,6};
		int N = arr.length;

		int rev[] = new int[N];
		for(int i=0; i<N; i++){

			rev[N-(i+1)] = arr[i];
		}

		for(int i=0; i<N; i++){

			System.out.print(rev[i]+"\t");
		}
	}
}
