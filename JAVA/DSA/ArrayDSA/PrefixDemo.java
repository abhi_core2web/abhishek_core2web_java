import java.util.*;

class PrefixDemo{

	static int[] prefix(int arr[],int N){

		for(int i=1; i<N; i++){
	
			arr[i] = arr[i-1] + arr[i];		//[-3,3,5,9,14,16,24,15,18,19]
		}
		return arr;
	}

	public static void main(String[] args){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N = 10;

		prefix(arr,N);		
		for(int i=0; i<N; i++){

			System.out.println(arr[i]);
		}
	}
}

