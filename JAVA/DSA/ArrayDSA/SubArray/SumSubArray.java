/*Given an array of size N.
int arr[] = new int[]{2,4,1,3};
1. Print the sum of every single subarray
2. Print the sum of every single subarray using prefix sum technique
3. Print the sum of every single subarray with time complexity O(N) & without using extra space
complexity(carry forward approach)
*/

class SumSubArray1 {

	public static void main(String[] args){

		int arr[] = new int[]{2,4,1,3};
		for(int i=0 ; i<arr.length; i++){
			for(int j=i ; j<arr.length; j++){
				int sum = 0;
				for(int k=i ; k<=j; k++){
					sum += arr[k];
				}
				System.out.println(sum);
			}
		}
	}
}
