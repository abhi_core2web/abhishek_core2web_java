/*Given an array of size N, find the total
sum of all subarray sums.
int arr[] = new int[]{1,2,3};
*/

class TotalSum {

	public static void main(String[] args){

		int arr[] = new int[]{1,2,3};
		int totalSum = 0;

		for(int i=0 ; i<arr.length; i++){

			int sum = 0;		
			for(int j=i ; j<arr.length; j++){
				sum += arr[j];
				totalSum += sum;
			}
		}
		System.out.println(totalSum);
	}
}
