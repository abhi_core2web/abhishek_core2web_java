/*
 *Que: Find the second maximun number in the array
 */

import java.util.Scanner;

class SecMax{

	static int secondMax(int arr[],int N){

		int max = Integer.MIN_VALUE;
		int secMax = Integer.MIN_VALUE;

		for(int i=0; i<N; i++){

			if(max < arr[i])
				max = arr[i];
		}

		for(int i=0; i<N; i++){
	
			if(secMax < arr[i]){
			
				if(secMax != max){
					secMax = arr[i];
				}
			}
		}
		return secMax;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array ");
		int N = sc.nextInt();

		System.out.println("Enter elemnt for array:");
		int arr[] = new int[N];

		for(int i=0; i<N; i++){

			arr[i] = sc.nextInt();
		}

		int secMax = secondMax(arr,N);

		System.out.println("Second largest number from the array is :"+secMax);
	
	}
}

