//Sliding window approach


/*
 * Given an array of size N.
 * Find the maximum subarray sun of length of k.
 * Arr[] = {-3,4,-2,5,3,-2,8-2,1,4}
 * k = 4;
 */


class SlidingWindow {
    public static void main(String[] args) {
        int arr[] = {-3,4,-2,5,3,-2,8,2,1,4};
        int k = 4;

        int start = 0;
        int end = k-1;
        int sum = 0;
        for(int i=start; i<=end; i++) {
            sum += arr[i];
        }
        int max = sum;
        start = 1;
        end = k;


        while(end <arr.length) {
            sum = sum + arr[end] - arr[start-1];
            if(sum > max) {
                max = sum;
            }
            start++;
            end++;
        }
        System.out.println("Maximum sum of length k :"+max);
    }
}