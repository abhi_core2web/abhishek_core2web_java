/*
 * Given an array of size N.
 * Find the maximum subarray sun of length of k.
 * Arr[] = {-3,4,-2,5,3,-2,8-2,1,4}
 * k = 4;
 */


import java.util.Scanner;

class MaxNumberLength {

    static void bruteForceApproch(int arr[], int k) {
        int start = 0;
        int end = k-1;
        int count = 0;
        int max = Integer.MIN_VALUE;
        
        while(end < arr.length) {
            int sum = 0;
            count++;
            for(int i=start; i<=end; i++) {
                sum += arr[i];
            }
            if(sum > max) 
                max = sum;

            start++;
            end++;
        }
        System.out.println("Total sub array of length k :"+count);
        System.out.println("Maximum sum of length k :"+max);

        //TC = O(N^2)
        //SC = O(1)
    }
    
    static void optimizedUsingPrefix(int arr[],int k) {

        int prefArr[] = new int[arr.length];

        prefArr[0] = arr[0];
        for(int i=1; i<arr.length; i++) {
            prefArr[i] = prefArr[i-1] + arr[i];

        }

        

        int start = 0;
        int end = k-1;
        int max = Integer.MIN_VALUE;
        
        while(end < arr.length) {
            int sum = 0;
            if(start == 0) {
                sum = prefArr[end];
            } else {
                sum = prefArr[end] - prefArr[start-1];
            }
            if(sum > max) 
                max = sum;
            start++;
            end++;
        }
        System.out.println("Maximum sum of length k :"+max);
        //TC = O(N)
        //SC = O(N)

    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array :");
        int n = sc.nextInt();
        int arr[] = new int[n];
        System.out.println("Enter the elements of array :");
        for(int i=0; i<n; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println("Enter the value of k :");
        
        int k = sc.nextInt();

        bruteForceApproch(arr,k);
        optimizedUsingPrefix(arr,k);
    }
}