class sqe{

        static int sqr(int num){
                int start =1;
                int end=num;
                int ans=0;
                int cnt=0;

                while(start<=end){
                        int mid=(start+end)/2;
                        if(mid*mid==num){
                                return mid;
                        }
                        if(mid*mid>num){
                                end=mid-1;
                        }
                        else{

                                ans=mid;
                                start=mid+1;
                        }

                }
                //System.out.println(ans);
                return ans;
        }
        public static void main(String[] args){
                int num=100;
                int k=sqr(num);
                System.out.println(k);
        }

}
