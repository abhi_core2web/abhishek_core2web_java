class ArrayRotate {

	static void rotation(int arr[], int n, int k){

		int x = 1;
		while(x<=k){

			int last = arr[0];
			for(int i=0; i<arr.length-1; i++){

				arr[i]= arr[i+1];
			}
			arr[arr.length-1] = last;
			x++;
		}
	}

	public static void main(String[] args){

		int arr[] = new int[]{2,4,5,4};
		int n = arr.length;
		int k = 2;

		for(int i=0; i<arr.length;i++){

			System.out.print(arr[i]+"\t");
		}
		System.out.println();
		rotation(arr,n,k);
		for(int i=0; i<arr.length;i++){

			System.out.print(arr[i]+"\t");
		}
	}
}
