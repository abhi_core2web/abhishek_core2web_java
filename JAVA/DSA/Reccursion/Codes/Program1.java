
class RecursionDemo1 {

    static void printNum(int n) {
        System.out.println(n);
        n--;
        if(n == 0) {
            return;
        }
        printNum(n);
    }
    public static void main(String[] args) {
        printNum(5);
    }
}