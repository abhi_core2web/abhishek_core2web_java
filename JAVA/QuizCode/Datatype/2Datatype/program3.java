class Demo3{
	
	public static void(String[] args){

		double d1 = 30.43d;
		double d2 = 'f';	//As the character is implicitly converted into it's ASCII value so it's a valid initialization
		double d3 = 132443331213l;	//Long also converted into the double so it is also the valid intialization.

		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
	}

}
