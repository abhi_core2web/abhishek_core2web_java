class Demo1{
	
	public static void main(String[] args){

		int ch1 = 65;
		char ch2 = ch1;		//As there int is about 32 byte and the char is 16 byte so there is no directly conversion is possible.

		System.out.println(ch1);
		System.out.println(ch2);
	}

}
