class ConstructorDemo{

	ConstructorDemo(){

		System.out.println("Welcome To Constructor");
	}
	public static void main(String[] args){
		
		ConstructorDemo obj = new ConstructorDemo();

		System.out.println("Welcome to Main");	//In this code if user not mention any constructor then Compiler add it.
	}
}
