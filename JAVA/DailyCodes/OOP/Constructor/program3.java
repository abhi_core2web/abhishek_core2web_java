class Demo{

	Demo(){

		System.out.println("In Demo Constructor");
	}

	void Demo(){

		System.out.println("In Demo Method");
	}

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.Demo();

	}
}
