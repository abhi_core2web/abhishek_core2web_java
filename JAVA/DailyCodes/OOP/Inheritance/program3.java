class Parent{
	
	int x = 10;
	int y = 20;

	Parent(){
		super();
		System.out.println("Parent Construtor");
	}

	void gun(){

		System.out.println("In gun");
	}
}

class Child extends Parent{

	public static void main(String[] args){

		Child obj = new Child();
		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}
