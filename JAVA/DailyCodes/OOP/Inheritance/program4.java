class Parent{
	
	int x = 10;
	int y = 20;

	Parent(){
		super();
		System.out.println("Parent Construtor");
	}

	void gun(){

		System.out.println("In gun Parent");
	}
}

class Child extends Parent{
	
	int x = 30;
	int y = 40;

	Child(){
		super();
		System.out.println("Child Construtor");
	}

	void gun(){

		System.out.println("In gun child");
	}
	
	
}

class Demo{

	public static void main(String[] args){

		Child obj = new Child();
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.gun();
	}
}
