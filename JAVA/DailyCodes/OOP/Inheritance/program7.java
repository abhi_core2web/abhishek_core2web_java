
//Main Universities and universities under colleges relationship


class University{
	
	University(){
		
		System.out.println("In University");
	}
	
	void paperPattern(){
		System.out.println("60 written and 40 Internel");
	}	

	void result(){

		System.out.println("Result date according to respective department");
	}
}

class College extends University{
	
	void placements(){

		System.out.println("Differnt placements");	
	}

	public static void main(String[] args){
	
		College obj = new College();

		obj.paperPattern();	//parent property
		obj.result();		//parent property
		obj.placements();	//child property
	}

}

