class ExceptionDemo extends Exception{
    
    ExceptionDemo(String s){
        super(s);
    }
}

class Theatre {

    int totalAvlSeats = 0;
    int seatsOccupied = 50;

    void bookTicket() throws ExceptionDemo {
        if (totalAvlSeats == 0) {
            throw new ExceptionDemo("The theatre is full");
        } else {
            totalAvlSeats--;
            seatsOccupied++;
            System.out.println("You have successfully booked a ticket");
        }
    }
}

class Client {

    public static void main(String[] args){
        Theatre theatre = new Theatre();
        try {
            theatre.bookTicket();
        } catch (ExceptionDemo e) {
            e.printStackTrace();
        }
    }
}


