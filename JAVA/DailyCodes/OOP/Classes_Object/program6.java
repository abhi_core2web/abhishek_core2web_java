class Demo{

	int x = 10;
	static String str1 = "Shashi";
	String str2 = "Rahul";
	String str3 = new String("Bagal");
	static String str4 = new String("Pitch");
	
	public static void main(String[] args){

		Demo obj = new Demo();
	
		System.out.println(obj.x);	
		System.out.println(obj.str1);	
		System.out.println(obj.str2);	
		System.out.println(obj.str3);	
		System.out.println(obj.str4);		
	}
}
