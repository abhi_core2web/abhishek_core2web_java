class Xyz{

	void fun(int n){	//For the method there is always need to be matched parameter and argumnt

		System.out.println("In fum");
	}
}

class Demo{

	void run(float f){

		System.out.println("In run");
	}

	public static void main(String[] args){

		Xyz obj1 = new Xyz();
		obj1.fun();
		Demo obj2 = new Demo();
		obj2.run();
	}
}
