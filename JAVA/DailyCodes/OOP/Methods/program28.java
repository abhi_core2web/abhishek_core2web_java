class Xyz{

	void fun(int n){	

		System.out.println("In fum");
		System.out.println(n);
	}
}

class Demo{

	void run(float f){

		System.out.println("In run");
		System.out.println(f);
	}

	public static void main(String[] args){

		Xyz obj1 = new Xyz();
		obj1.fun(10);
		Demo obj2 = new Demo();
		obj2.run(20.5);
	}
}
