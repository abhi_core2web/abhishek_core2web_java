class Xyz{

	void gun(){

		System.out.println("In gun");
	}
	void fun(int n){	

		System.out.println("In fum");
		System.out.println(n);
	}
}

class Demo{

	void run(int i, float f){

		System.out.println("In run");
		System.out.println(i);
		System.out.println(f);
	}

	public static void main(String[] args){

		Xyz obj1 = new Xyz();
		obj1.fun(10);
		obj1.gun();

		Demo obj2 = new Demo();
		obj2.run(10);
	}
}
