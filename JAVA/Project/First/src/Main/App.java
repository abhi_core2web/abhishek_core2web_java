package Main;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application{
    public static void main(String[] args){
        System.out.println("Hello, World!");
        launch(args);
    }

    @Override
    public void start(Stage firstStage){

        firstStage.initStyle(StageStyle.DECORATED);
        firstStage.show();
        firstStage.setTitle("My First javaFx Code..");
        firstStage.getIcons().add(new Image("assets/images/eye.png"));
      }
}
