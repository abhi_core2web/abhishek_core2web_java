import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class App extends Application {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        launch(args);
    }

    @Override
    public void start(Stage mystage) {
        mystage.setTitle("Assignment-1");
        mystage.setHeight(800);
        mystage.setWidth(1100);
        mystage.centerOnScreen();

        Text txt1 = new Text("Name:- Abhishek Bhosale");
        txt1.setFont(Font.font(20));

        Text txt2 = new Text("Hobbies:- Coding, Swimming");
        txt2.setFont(Font.font(20));

        Text txt3 = new Text("Languages:- Java, C, C++, Python");
        txt3.setFont(Font.font(20));

        VBox vbox = new VBox(20, txt1, txt2, txt3);
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox);
        scene.setCursor(Cursor.TEXT);
        scene.setFill(Color.AQUA); // Set scene background color

        mystage.setScene(scene);
        mystage.show();
    }
}
