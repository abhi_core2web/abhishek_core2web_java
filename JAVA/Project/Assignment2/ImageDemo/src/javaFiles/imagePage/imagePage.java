package javaFiles.imagePage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class imagePage extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Image Page");

        Image img1 = new Image("assets/images/java.png");
        Image img2 = new Image("assets/images/python.png");
        Image img3 = new Image("assets/images/c.jpeg");
        Image img4 = new Image("assets/images/cpp.jpeg");

        Image img5 = new Image("assets/images/spring.png");
        Image img6 = new Image("assets/images/reactjs.jpeg");
        Image img7 = new Image("assets/images/Flutter.jpeg");
        Image img8 = new Image("assets/images/node.png");

        ImageView imgView1 = new ImageView(img1);
        imgView1.setFitWidth(230); 
        imgView1.setFitHeight(230); 
        imgView1.setPreserveRatio(true);

        ImageView imgView2 = new ImageView(img2);
        imgView2.setFitWidth(230); 
        imgView2.setFitHeight(230); 
        imgView2.setPreserveRatio(true);

        ImageView imgView3 = new ImageView(img3);
        imgView3.setFitWidth(230); 
        imgView3.setFitHeight(230); 
        imgView3.setPreserveRatio(true);

        ImageView imgView4 = new ImageView(img4);
        imgView4.setFitWidth(230); 
        imgView4.setFitHeight(230); 
        imgView4.setPreserveRatio(true);

        // Creating image views for frameworks
        ImageView imgView5 = new ImageView(img5);
        imgView5.setFitWidth(230); 
        imgView5.setFitHeight(230); 
        imgView5.setPreserveRatio(true);

        ImageView imgView6 = new ImageView(img6);
        imgView6.setFitWidth(230); 
        imgView6.setFitHeight(230); 
        imgView6.setPreserveRatio(true);

        ImageView imgView7 = new ImageView(img7);
        imgView7.setFitWidth(230); 
        imgView7.setFitHeight(230); 
        imgView7.setPreserveRatio(true);

        ImageView imgView8 = new ImageView(img8);
        imgView8.setFitWidth(230); 
        imgView8.setFitHeight(230); 
        imgView8.setPreserveRatio(true);


        Label label1 = new Label("Java");
        label1.setStyle("-fx-font-size: 100px;"); 

        Label label2 = new Label("Python");
        label2.setStyle("-fx-font-size: 100px;"); 

        Label label3 = new Label("C");
        label3.setStyle("-fx-font-size: 100px;"); 

        Label label4 = new Label("CPP");
        label4.setStyle("-fx-font-size: 100px;"); 

        Label label5 = new Label("Spring");
        label5.setStyle("-fx-font-size: 100px;"); 

        Label label6 = new Label("React");
        label6.setStyle("-fx-font-size: 100px;"); 

        Label label7 = new Label("Flutter");
        label7.setStyle("-fx-font-size: 100px;"); 

        Label label8 = new Label("Node");
        label8.setStyle("-fx-font-size: 100px;"); 


        HBox hBox1 = new HBox(100, imgView1, label1);
        hBox1.setStyle("-fx-background-color: cyan;");
        hBox1.setAlignment(Pos.CENTER);

        HBox hBox2 = new HBox(100, imgView2, label2);
        hBox2.setStyle("-fx-background-color: orange;");
        hBox2.setAlignment(Pos.CENTER);

        HBox hBox3 = new HBox(100, imgView3, label3);
        hBox3.setStyle("-fx-background-color: teal;");
        hBox3.setAlignment(Pos.CENTER);

        HBox hBox4 = new HBox(100, imgView4, label4);
        hBox4.setStyle("-fx-background-color: pink;");
        hBox4.setAlignment(Pos.CENTER);

        HBox hBox5 = new HBox(100, imgView5, label5);
        hBox5.setStyle("-fx-background-color: cyan;");
        hBox5.setAlignment(Pos.CENTER);

        HBox hBox6 = new HBox(100, imgView6, label6);
        hBox6.setStyle("-fx-background-color: orange;");
        hBox6.setAlignment(Pos.CENTER);

        HBox hBox7 = new HBox(100, imgView7, label7);
        hBox7.setStyle("-fx-background-color: teal;");
        hBox7.setAlignment(Pos.CENTER);

        HBox hBox8 = new HBox(100, imgView8, label8);
        hBox8.setStyle("-fx-background-color: pink;");
        hBox8.setAlignment(Pos.CENTER);


        VBox vBoxLeft = new VBox(20, hBox1, hBox2, hBox3, hBox4);
        vBoxLeft.setAlignment(Pos.CENTER);

        VBox vBoxRight = new VBox(20, hBox5, hBox6, hBox7, hBox8);
        vBoxRight.setAlignment(Pos.CENTER);

        HBox hBoxMain = new HBox(20, vBoxLeft, vBoxRight);
        hBoxMain.setStyle("-fx-background-color: lightgrey;");
        hBoxMain.setAlignment(Pos.CENTER);

        Scene scene = new Scene(hBoxMain, 900, 800, Color.BLACK);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
