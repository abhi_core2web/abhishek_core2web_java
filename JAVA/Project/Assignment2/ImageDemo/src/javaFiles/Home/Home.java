package javaFiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Home extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Pane, StackPane, BorderPane");
        stage.setHeight(1030);
        stage.setWidth(1500);

        BorderPane br = new BorderPane();

        Text txt1 = new Text("Right Top");
        Text txt2 = new Text("Left Top");
        Text txt3 = new Text("Right Bottom");
        Text txt4 = new Text("Right Bottom");

        VBox vbox1 = new VBox(txt1);
        VBox vbox2 = new VBox(txt2);
        VBox vbox3 = new VBox(txt3);
        VBox vbox4 = new VBox(txt4);

        br.setRight(vbox1);
        br.setLeft(vbox2);
        br.setBottom(vbox3);
        br.setRight(vbox4);

        Label label1 = new Label("Welcome");
        label1.setAlignment(Pos.CENTER);
        br.setCenter(label1);

        br.setStyle("-fx-background-color: green;");

        Scene scene = new Scene(br);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
