package JavaFiles.Home;



import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;

public class main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Label label1 = new Label("Enter Username ");
        label1.setStyle("-fx-text-fill: red;");

        Label label2 = new Label("Enter Password ");
        label2.setStyle("-fx-text-fill: blue;");

        TextField tx = new TextField();
        tx.setMaxHeight(300);
        tx.setMaxWidth(400);
        tx.setStyle("-fx-background-color: yellow;");

        PasswordField ps = new PasswordField();
        ps.setMaxHeight(300);
        ps.setMaxWidth(400);
        ps.setStyle("-fx-background-color: pink;");

        Button btn = new Button("Show");
        btn.setStyle("-fx-background-color: green; -fx-text-fill: white;");

        Label username = new Label();
        username.setStyle("-fx-text-fill: purple;");

        Label password = new Label();
        password.setStyle("-fx-text-fill: orange;");

        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(tx.getText());
                System.out.println(ps.getText());
                username.setText("Username: " + tx.getText());
                password.setText("Password: " + ps.getText());
            }
        });

        Border border = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2)));
        VBox vb = new VBox(30, label1, tx, label2, ps, btn, username, password);
        vb.setAlignment(Pos.CENTER);
        vb.setBorder(border);
        vb.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, null)));
    
        Scene scene = new Scene(vb, 400, 600);

        primaryStage.setTitle("JavaFX Application");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
