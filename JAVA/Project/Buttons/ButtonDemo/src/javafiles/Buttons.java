package javafiles;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Buttons extends Application {
    
    @Override
    public void start(Stage stage) {
        stage.setTitle("Buttons");

        Button initialButton = new Button("hello Super-X");
        initialButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("Core2Web!");
            }
        });

        Separator separator1 = new Separator();
        separator1.setStyle("-fx-background-color: Black;");

        Label core2WebLabel1 = new Label("Core2Web.in");
        core2WebLabel1.setStyle("-fx-font-size: 20; -fx-font-weight: bolder");

        Button javaButton = new Button("Core2Web-Java");
        javaButton.setStyle("-fx-font-weight: bolder");
        javaButton.setPrefWidth(180);
        javaButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("java-2024");
                javaButton.setStyle("-fx-background-color: Green");
            }
        });

        Button superXButton = new Button("Core2Web-Super-X");
        superXButton.setStyle("-fx-font-weight: bolder");
        superXButton.setPrefWidth(180);
        superXButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("Super-X-2024");
                superXButton.setStyle("-fx-background-color: Green");
            }
        });

        Button dsaButton = new Button("Core2Web-DSA");
        dsaButton.setStyle("-fx-font-weight: bolder");
        dsaButton.setPrefWidth(180);
        dsaButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("DSA-2024");
                dsaButton.setStyle("-fx-background-color: Green");
            }
        });

        VBox core2WebVBox = new VBox(40, javaButton, superXButton, dsaButton);
        core2WebVBox.setAlignment(Pos.CENTER);


        Separator separator2 = new Separator();
        separator2.setStyle("-fx-background-color: Black;");

        Label core2WebLabel2 = new Label("Core2Web.in");

        Button osButton = new Button("OS");
        osButton.setStyle("-fx-background-color: blue;");
        osButton.setTextFill(Color.WHITE);
        osButton.setFont(new Font(20));

        Button cButton = new Button("C");
        cButton.setStyle("-fx-background-color: blue;");
        cButton.setTextFill(Color.WHITE);
        cButton.setFont(new Font(20));

        Button cppButton = new Button("CPP");
        cppButton.setStyle("-fx-background-color: blue;");
        cppButton.setTextFill(Color.WHITE);
        cppButton.setFont(new Font(20));

        HBox hbox1 = new HBox(20, osButton, cButton, cppButton);
        hbox1.setStyle("-fx-font-weight: Bold");
        hbox1.setAlignment(Pos.CENTER);

        Button javaLangButton = new Button("Java");
        javaLangButton.setStyle("-fx-background-color: blue;");
        javaLangButton.setTextFill(Color.WHITE);
        javaLangButton.setFont(new Font(20));

        Button pythonButton = new Button("Python");
        pythonButton.setStyle("-fx-background-color: blue;");
        pythonButton.setTextFill(Color.WHITE);
        pythonButton.setFont(new Font(20));

        Button dsaLangButton = new Button("DSA");
        dsaLangButton.setStyle("-fx-background-color: blue;");
        dsaLangButton.setTextFill(Color.WHITE);
        dsaLangButton.setFont(new Font(20));

        HBox hbox2 = new HBox(20, javaLangButton, pythonButton, dsaLangButton);
        hbox2.setStyle("-fx-font-weight: Bold");
        hbox2.setAlignment(Pos.CENTER);

        VBox root = new VBox(50, initialButton,separator1, core2WebLabel1, core2WebVBox, separator2, core2WebLabel2, hbox1, hbox2);
        root.setStyle("-fx-background-color: Yellow;");
        root.setAlignment(Pos.CENTER);

        Scene scene = new Scene(root, 500, 500);
        stage.setScene(scene);
        stage.show();
    }
}
