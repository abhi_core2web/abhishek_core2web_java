package javafiles;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MonthDemo extends Application{
    
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Month Demo");

        Button btn1 = new Button("January");
        configureButton(btn1);
        Button btn2 = new Button("February");
        configureButton(btn2);
        Button btn3 = new Button("March");
        configureButton(btn3);
        Button btn4 = new Button("April");
        configureButton(btn4);
        Button btn5 = new Button("May");
        configureButton(btn5);
        Button btn6 = new Button("June");
        configureButton(btn6);
        Button btn7 = new Button("July");
        configureButton(btn7);
        Button btn8 = new Button("August");
        configureButton(btn8);
        Button btn9 = new Button("September");
        configureButton(btn9);
        Button btn10 = new Button("October");
        configureButton(btn10);
        Button btn11 = new Button("November");
        configureButton(btn11);
        Button btn12 = new Button("December");
        configureButton(btn12);

        VBox months = new VBox(20, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12);
        months.setAlignment(Pos.TOP_LEFT);
        months.setStyle("-fx-background-color: yellow");
        months.setPadding(new Insets(100)); 
        
        TextField tx = new TextField();
        Button print = new Button("Print Text");
        configureButton(print);

        print.setOnAction(new EventHandler<ActionEvent>() {
            
            public void handle(ActionEvent event) {
                System.out.println(tx.getText());
            }
        });
    
        VBox printTxt = new VBox(30,tx,print);
        printTxt.setAlignment(Pos.CENTER_RIGHT);
        printTxt.setStyle("-fx-background-color: green");
        printTxt.setPadding(new Insets(100));

        HBox root = new HBox(1000,months, printTxt);

        Scene scene = new Scene(root, 400, 400);
        stage.setScene(scene);
        stage.show();
    }

    private void configureButton(Button btn){
        btn.setPrefWidth(120);
        btn.setTextFill(Color.WHITE);
        btn.setBackground(new Background(new BackgroundFill(Color.BLUE, new CornerRadii(5), Insets.EMPTY)));
        btn.setStyle("-fx-font-size: 20px -fx- font-weight:Bold");
        btn.setAlignment(Pos.TOP_LEFT);
    }
    public static void main(String[] args) {
        launch(args);
    }
}
