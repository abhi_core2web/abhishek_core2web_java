package javafiles;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Funtionalities extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Functionalities");

        BorderPane br = new BorderPane();  
        
        // 1-Vbox
        TextField number = new TextField();
        configureTextField(number);
        Button sqr = new Button("Square");
        Button sqrt = new Button("Square Root");

        VBox operation = new VBox(30, number, sqr, sqrt);
        configureVbox(operation);
        configureButton(sqrt);
        configureButton(sqr);
        operation.setStyle("-fx-background-color: yellow");
        br.setLeft(operation);

        // 2-Vbox
        Label usr = new Label("Enter Username");
        Label pass = new Label("Enter Password");
        TextField username = new TextField();
        configureTextField(username);
        PasswordField password = new PasswordField();
        configureTextField(password);
        Button login = new Button("Verify");

        VBox verify = new VBox(30, usr, username, pass, password, login);
        verify.setStyle("-fx-background-color: orange");
        configureVbox(verify);
        configureButton(login);
        br.setRight(verify);

        // 3-Vbox
        TextField pal = new TextField();
        configureTextField(pal);
        Button check = new Button("Check Palindrome");

        VBox palindrome = new VBox(30, pal, check);
        palindrome.setStyle("-fx-background-color: green");
        configureVbox(palindrome);
        configureButton(check);
        br.setBottom(palindrome);

        // 4-Vbox
        PasswordField chkPass = new PasswordField();
        configureTextField(chkPass);
        Button show = new Button("Show Password");

        VBox print = new VBox(20, chkPass, show);
        print.setStyle("-fx-background-color: blue");
        configureVbox(print);
        configureButton(show);
        br.setTop(print);
        
        // HBox for Output
        Label out = new Label("O/P: ");
        TextField output = new TextField();
        configureTextField(output);
        HBox hb = new HBox(20, out, output);
        hb.setAlignment(Pos.CENTER);
        br.setCenter(hb);

        // Button
        sqr.setOnAction(new EventHandler<ActionEvent>() {
           
            public void handle(ActionEvent event) {
                int num = Integer.parseInt(number.getText());
                output.setText(num * num + "");
            }
        });

        sqrt.setOnAction(new EventHandler<ActionEvent>() {
           
            public void handle(ActionEvent event) {
                double num = Double.parseDouble(number.getText());
                output.setText(String.valueOf(Math.sqrt(num)));
            }
        });

        login.setOnAction(new EventHandler<ActionEvent>() {
         
            public void handle(ActionEvent event) {
                String user = username.getText();
                String pass = password.getText();

                if (user.equals("Admin") && pass.equals("Admin123")) {
                    output.setText("Login Successful");
                } else {
                    output.setText("Login Failed");
                }
            }
        });

        check.setOnAction(new EventHandler<ActionEvent>() {
                
                public void handle(ActionEvent event) {
                    String text = pal.getText();
                    if (isPalindrome(text)) {
                        output.setText("Palindrome");
                    } else {
                        output.setText("Not Palindrome");
                    }
                }
            });

        show.setOnAction(new EventHandler<ActionEvent>() {
                
                public void handle(ActionEvent event) {
                    output.setText(chkPass.getText());
                }
            
        });

        Scene scene = new Scene(br, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    public void configureVbox(VBox vbox) {
        vbox.setPrefHeight(200);
        vbox.setPrefWidth(500);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(30));
    }

    public void configureButton(Button btn) {
        btn.setPrefWidth(150);
        btn.setBackground(new Background(new BackgroundFill(Color.AQUA, new CornerRadii(10), Insets.EMPTY)));
        btn.setFont(new Font("bold", 16));
        btn.setCursor(Cursor.HAND);
    }

    public void configureTextField(TextField txt) {
        txt.setMinWidth(150);
        txt.setMinHeight(30);
        txt.setFont(new Font("Arial", 16));
    }

    public boolean isPalindrome(String text) {
        int n = text.length();
        for (int i = 0; i < n / 2; i++) {
            if (text.charAt(i) != text.charAt(n - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
