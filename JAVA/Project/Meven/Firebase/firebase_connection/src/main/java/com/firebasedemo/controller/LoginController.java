package com.firebasedemo.controller;

import com.firebasedemo.firebase_connetion.FirebaseService;
import com.firebasedemo.firebase_connetion.OAuthService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;

public class LoginController extends Application {
    
    private Stage primaryStage;
    private Scene scene;
    private FirebaseService firebaseService;
    private OAuthService oAuthService;

    public void setPrimaryStageScene(Scene scene) {
        primaryStage.setScene(scene);
    }

    public void initializeLoginScene() {
        Scene loginScene = createLoginScene();
        this.scene = loginScene;
        primaryStage.setScene(loginScene);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        
        try {
            FileInputStream serviceAccount = new FileInputStream("F:\\maven\\Firebase\\firebase_connection\\src\\main\\resources\\fx-firestore.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://java-fx-fire-store-default-rtdb.firebaseio.com/")
                .build();
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            e.printStackTrace();
        }

        firebaseService = new FirebaseService(this, new TextField(), new PasswordField());
        oAuthService = new OAuthService();

        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Scene createLoginScene() {

        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");

        Button signUpButton = new Button("Sign Up");
        Button loginButton = new Button("Login");
        Button googleSignInButton = new Button("Sign in with Google");
        Button facebookSignInButton = new Button("Sign in with Facebook");

        firebaseService = new FirebaseService(this, emailField, passwordField);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                firebaseService.signUp();
            }
        });

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
            }
        });

        googleSignInButton.setOnAction(e -> oAuthService.signInWithGoogle());
        facebookSignInButton.setOnAction(e -> oAuthService.signInWithFacebook());

        VBox fieldBox = new VBox(20, emailField, passwordField);
        HBox buttonBox = new HBox(20, signUpButton, loginButton);
        VBox oauthBox = new VBox(20, googleSignInButton, facebookSignInButton);
        VBox comBox = new VBox(20, fieldBox, buttonBox, oauthBox);
        Pane viewPane = new Pane(comBox);

        return new Scene(viewPane, 400, 300);
    }
}
