package com.firebasedemo.firebase_connetion;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.firebasedemo.controller.LoginController;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class FirebaseService {

    private TextField emailField;
    private PasswordField passwordField;
    private LoginController loginController;

    public FirebaseService(LoginController loginController, TextField emailField, PasswordField passwordField) {
        this.emailField = emailField;
        this.passwordField = passwordField;
        this.loginController = loginController;
    }

    public boolean signUp() {
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false);

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created new user: " + userRecord.getUid());
            showAlert("Success", "User Created Successfully");
            return true;
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            showAlert("Error", "User Creation Failed: " + e.getMessage());
            return false;
        }
    }

    public boolean login() {
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            String apiKey = "AIzaSyDrv0za-ljbAPAIAXvf8rX4F7MWXnQUrh4"; // Replace with your Firebase Web API key

            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + apiKey);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("email", email);
            jsonRequest.put("password", password);
            jsonRequest.put("returnSecureToken", true);

            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            if (conn.getResponseCode() == 200) {
                showAlert(true);
                return true;
            } else {
                showAlert("Invalid Login", "Invalid Credentials");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlert(false);
            return false;
        }
    }

    private void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(message);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void showAlert(boolean isLoggedIn) {
        // Show different UI elements based on login status
    }
}
