package com.firebasedemo.firebase_connetion;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class OAuthApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("OAuth Example");

        Button googleSignInButton = new Button("Sign in with Google");
        googleSignInButton.setOnAction(e -> signInWithGoogle(primaryStage));

        VBox vbox = new VBox(googleSignInButton);
        Scene scene = new Scene(vbox, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void signInWithGoogle(Stage stage) {
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();

        // URL for Firebase authentication; replace with your own Firebase project URL
        String url = "https://java-fx-fire-store.firebaseapp.com/__/auth/handler?provider=google.com";
        webEngine.load(url);

        // Create a new Stage for the WebView
        Stage webViewStage = new Stage();
        webViewStage.setTitle("Sign in with Google");
        webViewStage.setScene(new Scene(webView, 800, 600));
        webViewStage.show();
        
        // Handle OAuth token retrieval and sign-in processing here
        // This might involve listening for redirect URL changes or handling post-authentication logic
    }

    public static void main(String[] args) {
        launch(args);
    }
}
