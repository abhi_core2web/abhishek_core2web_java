package com.firebasedemo;

import com.firebasedemo.controller.LoginController;
import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(LoginController.class, args);
    }
}