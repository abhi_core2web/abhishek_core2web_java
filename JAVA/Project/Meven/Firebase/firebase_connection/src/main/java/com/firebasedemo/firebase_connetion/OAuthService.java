package com.firebasedemo.firebase_connetion;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import javafx.scene.control.Alert;

import java.io.FileInputStream;
import java.io.IOException;

public class OAuthService {

    public OAuthService() {
        
    }

    public void signInWithGoogle() {
        OAuthProvider provider = OAuthProvider.newBuilder("google.com").build();

        FirebaseAuth.getInstance().signInWithCredential(provider)
            .addOnSuccessListener(result -> {
                UserRecord user = result.getUser();
                System.out.println("User signed in: " + user.getUid());
                showAlert("Success", "User signed in with Google.");
            })
            .addOnFailureListener(e -> {
                e.printStackTrace();
                showAlert("Error", "Google sign-in failed: " + e.getMessage());
            });
    }

    public void signInWithFacebook() {
        OAuthProvider provider = OAuthProvider.newBuilder("facebook.com").build();

        FirebaseAuth.getInstance().signInWithCredential(provider)
            .addOnSuccessListener(result -> {
                UserRecord user = result.getUser();
                System.out.println("User signed in: " + user.getUid());
                showAlert("Success", "User signed in with Facebook.");
            })
            .addOnFailureListener(e -> {
                e.printStackTrace();
                showAlert("Error", "Facebook sign-in failed: " + e.getMessage());
            });
    }

    private void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(message);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
