package com.pojo;

public class Player {
    
    private String playerName;
    private String PlayerTeam;
    private int jerNo;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerTeam() {
        return PlayerTeam;
    }

    public void setPlayerTeam(String PlayerTeam) {
        this.PlayerTeam = PlayerTeam;
    }

    public int getJerNo() {
        return jerNo;
    }

    public void setJerNo(int jerNo) {
        this.jerNo = jerNo;
    }
}
