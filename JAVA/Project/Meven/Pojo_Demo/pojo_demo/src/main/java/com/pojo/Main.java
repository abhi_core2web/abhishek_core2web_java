package com.pojo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        Player obj = new Player();
        System.out.println("Enter player name: ");
        obj.setPlayerName(sc.nextLine());
        System.out.println("Enter player team: ");
        obj.setPlayerTeam(sc.nextLine());
        System.out.println("Enter player jersey number: ");
        obj.setJerNo(sc.nextInt());

        System.out.println("Player name: " + obj.getPlayerName());
        System.out.println("Player team: " + obj.getPlayerTeam());
        System.out.println("Player jersey number: " + obj.getJerNo());
        
    }
}