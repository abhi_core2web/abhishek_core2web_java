package com.googlesignin;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class JavaFXAdvancedCSSDemo extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Label with advanced CSS
        Label label = new Label("Advanced CSS Styling");
        label.setStyle(
            "-fx-font-size: 30px;" +
            "-fx-text-fill: linear-gradient(from 0% 0% to 100% 100%, #ff7f50, #ff4500);" +
            "-fx-background-color: radial-gradient(center 50% 50%, #fff, #ddd);" +
            "-fx-padding: 10;" +
            "-fx-border-color: #ff4500;" +
            "-fx-border-width: 3;" +
            "-fx-border-radius: 15;" +
            "-fx-background-radius: 10;" +
            "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.7), 10, 0, 0, 5);"
        );

        // Button with advanced CSS
        Button button = new Button("Click Me");
        button.setStyle(
            "-fx-background-color: linear-gradient(#ff7f50, #ff4500);" +
            "-fx-background-radius: 20;" +
            "-fx-text-fill: white;" +
            "-fx-font-size: 16px;" +
            "-fx-padding: 10 20;" +
            "-fx-border-color: #ff4500;" +
            "-fx-border-radius: 20;" +
            "-fx-border-width: 2;" +
            "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.5), 8, 0, 0, 5);"
        );

        // ImageView with CSS effects
        Image image = new Image("https://via.placeholder.com/150");
        ImageView imageView = new ImageView(image);
        imageView.setStyle(
            "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.5), 15, 0, 0, 10);" +
            "-fx-clip: circle(75, 75, 75);" // Using clip for circular image
        );

        // VBox layout
        VBox vbox = new VBox(20);
        vbox.setPadding(new Insets(20));
        vbox.getChildren().addAll(label, button, imageView);
        vbox.setStyle(
            "-fx-background-color: #f0f8ff;" +
            "-fx-background-radius: 20;" +
            "-fx-border-color: #4682b4;" +
            "-fx-border-width: 3;" +
            "-fx-border-radius: 20;"
        );

        // Scene and Stage setup
        Scene scene = new Scene(vbox, 400, 500);
        primaryStage.setTitle("JavaFX Advanced CSS Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
