package com.googlesignin;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RealTimeClockApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Label to display date and time
        Label dateTimeLabel = new Label();
        
        // Define the date and time format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // Timeline to update the label every second
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, event -> {
            LocalDateTime currentTime = LocalDateTime.now();
            dateTimeLabel.setText(currentTime.format(formatter));
        }),
        new KeyFrame(Duration.seconds(1))); // Update every second
        clock.setCycleCount(Timeline.INDEFINITE); // Run indefinitely
        clock.play(); // Start the timeline

        // Set up the scene and stage
        StackPane root = new StackPane(dateTimeLabel);
        Scene scene = new Scene(root, 300, 200);
        primaryStage.setTitle("Real-Time Clock");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
