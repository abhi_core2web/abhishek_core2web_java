package com.googlesignin;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FirebaseAuthJavaFX extends Application {

    @Override
    public void start(Stage primaryStage) {
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load("https://your-web-app-url.com"); // Replace with your hosted HTML URL

        Button signOutButton = new Button("Sign Out");
        signOutButton.setOnAction(e -> {
            // Implement sign out logic here
        });

        VBox root = new VBox();
        root.getChildren().addAll(webView, signOutButton);

        Scene scene = new Scene(root, 800, 600);
        primaryStage.setTitle("Firebase Authentication");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
