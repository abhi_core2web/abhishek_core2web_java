package com.googlesignin;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.effect.GaussianBlur;
import javafx.stage.Stage;

public class LoaderExample extends Application {
    private boolean isLoaderVisible = false;
    private StackPane root;

    @Override
    public void start(Stage primaryStage) {
        // Circular Loader
        ProgressIndicator circularLoader = new ProgressIndicator();
        circularLoader.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS); // Indeterminate progress
        circularLoader.setStyle("-fx-progress-color: #FF5722;"); // Orange color
        circularLoader.setVisible(false); // Initially hidden

        // Button to show loader and blur background
        Button showLoaderButton = new Button("Show Loader");
        showLoaderButton.setOnAction(e -> {
            isLoaderVisible = !isLoaderVisible;
            circularLoader.setVisible(isLoaderVisible);
            showLoaderButton.setVisible(!isLoaderVisible);

            // Apply or remove the GaussianBlur effect
            if (isLoaderVisible) {
                root.setEffect(new GaussianBlur(10)); // Apply blur effect
            } else {
                root.setEffect(null); // Remove blur effect
            }
        });

        // Layout
        VBox layout = new VBox(30); // 30px spacing between elements
        layout.setStyle("-fx-padding: 20; -fx-alignment: center;");
        layout.getChildren().addAll(showLoaderButton, circularLoader);

        // Background root pane
        root = new StackPane();
        root.getChildren().add(layout);

        // Scene
        Scene scene = new Scene(root, 400, 200);
        primaryStage.setTitle("Loader Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
