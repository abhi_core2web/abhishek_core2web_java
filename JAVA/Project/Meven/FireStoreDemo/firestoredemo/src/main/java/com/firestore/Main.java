package com.firestore;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.firestore.FireStoreInit.FireBaseInit;
import com.firestore.initialize.InitializeApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        System.out.println( "Hello world!");
        Application.launch(InitializeApp.class, args);
        
    }
}