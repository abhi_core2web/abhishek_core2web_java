package com.firestore.FireStoreInit;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

public class FireBaseInit {
    private static Firestore db;

    public void initFirebase() throws IOException{
        
        FileInputStream serviceAccount = new FileInputStream("firestoredemo/src/main/resources/fx-firestore.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()   
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))   
                .build();

            FirebaseApp.initializeApp(options);     

            db = FirestoreClient.getFirestore();   
    }

    public void createRec() throws ExecutionException, InterruptedException{

        Map<String,Object>teamData = new HashMap<>();
        teamData.put("team", "India");
        teamData.put("Captain", "Rohit Sharma");
        teamData.put("manOfMatch","Virat Kohli");

        ApiFuture<WriteResult> future =db.collection("T20WCC").document("Winner").set(teamData);
        System.out.println("Update Time: "+future.get().getUpdateTime());
    }

    public void readRec() throws ExecutionException, InterruptedException{
    
        DocumentReference docRef = db.collection("T20WCC").document("Winner");
        ApiFuture<DocumentSnapshot> snapShot = docRef.get();

        DocumentSnapshot docSnap = snapShot.get();

        if(docSnap.exists()){
            System.out.println(docSnap.get("team"));    
            System.out.println(docSnap.get("Captain"));
            System.out.println(docSnap.get("manOfMatch"));
        }else{
            System.out.println("No such document");
        }
    }

    public void updateRec() throws InterruptedException, ExecutionException {
        
        Map<String,Object> updateData = new HashMap<>();
        updateData.put("manOfSeries", "J.Brumrah");

        ApiFuture<WriteResult> future = db.collection("T20WCC").document("Winner").set(updateData, SetOptions.merge());
        System.out.println("Update Time: "+future.get().getUpdateTime());

    }

    public void deleteRec() throws InterruptedException, ExecutionException {
        ApiFuture<WriteResult> future = db.collection("T20WCC").document("Winner").delete();
        System.out.println("Update Time: "+future.get().getUpdateTime());

    }

}
