package com.firestore.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.concurrent.ExecutionException;
import com.firestore.dashboards.UserPage;
import com.firestore.firebaseConfig.DataService;

public class LoginController {

    private Stage primaryStage; 
    private Scene loginScene; 
    private Scene userScene; 
    private DataService dataService; 
    public static String key; 

    public LoginController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        dataService = new DataService(); 
        initScenes(); 
    }

    private void initScenes() {
        initLoginScene(); 
    }

    private void initLoginScene() {
        Label userLabel = new Label("Username"); 
        TextField userTextField = new TextField(); 
        Label passLabel = new Label("Password"); 
        PasswordField passField = new PasswordField(); 

        Button loginButton = new Button("Login"); 
        Button signupButton = new Button("Signup"); 

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleLogin(userTextField.getText(), passField.getText()); 
                userTextField.clear(); 
                passField.clear(); 
            }
        });

        signupButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showSignupScene(); 
                userTextField.clear(); 
                passField.clear(); 
            }
        });

        userLabel.setStyle("-fx-text-fill: white;");
        passLabel.setStyle("-fx-text-fill: white;");

        VBox fieldBox1 = new VBox(10, userLabel, userTextField);
        fieldBox1.setMaxSize(300, 30);
        VBox fieldBox2 = new VBox(10, passLabel, passField);
        fieldBox2.setMaxSize(300, 30);
        HBox buttonBox = new HBox(50, loginButton, signupButton);
        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);

        userTextField.setStyle("-fx-pref-width:350");
        passField.setStyle("-fx-pref-width:350");

        VBox vbox = new VBox(20, fieldBox1, fieldBox2, buttonBox);
        vbox.setAlignment(Pos.CENTER);
        loginScene = new Scene(vbox, 600, 600);
    }

    private void initUserScene() {
        UserPage userPage = new UserPage(dataService); // Create UserPage instance
        userScene = new Scene(userPage.createUserScene(this::handleLogout), 600, 600); // Create user scene
    }

    public Scene getLoginScene() {
        return loginScene;
    }

    public void showLoginScene() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
        primaryStage.show();
    }

    private void handleLogin(String username, String password) {
        try {
            if (dataService.authenticateUser(username, password)) {
                key = username; 
                initUserScene();
                primaryStage.setScene(userScene); 
                primaryStage.setTitle("User Dashboard");
            } else {
                System.out.println("Invalid credentials");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void showSignupScene() {
        SignUpController signupController = new SignUpController(this); // Create SignupController instance
        Scene signupScene = signupController.createSignupScene(primaryStage); // Create signup scene
        primaryStage.setScene(signupScene);
        primaryStage.setTitle("Signup");
        primaryStage.show();
    }

    private void handleLogout() {
        primaryStage.setScene(loginScene); // Show login scene
        primaryStage.setTitle("Login");
    }
}