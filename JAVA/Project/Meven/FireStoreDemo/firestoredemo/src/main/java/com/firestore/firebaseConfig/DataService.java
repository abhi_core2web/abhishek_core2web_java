package com.firestore.firebaseConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import javafx.stage.FileChooser;

import com.google.cloud.firestore.WriteResult;
import com.google.cloud.storage.Bucket;

public class DataService {
  
    private static Firestore db;

    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("firestoredemo/src/main/resources/fx-firestore.json");

        @SuppressWarnings("deprecation")
        FirebaseOptions options = new FirebaseOptions.Builder()     // Use the FirebaseOptions.Builder class to create a FirebaseOptions object
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))   // Set the credentials for the FirebaseOptions object
            .build();

        FirebaseApp.initializeApp(options);     // Initialize the Firebase app with the FirebaseOptions object

        db = FirestoreClient.getFirestore();    // Get the Firestore database client
    }
    public void addData(String collection, String document, Map<String, Object> data) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection(collection).document(document);    // Create a reference to a document
        ApiFuture<WriteResult> result = docRef.set(data);   // Set the data in the document
        result.get();   // Wait for the result
    }
    
    public DocumentSnapshot getData(String collection, String document) throws ExecutionException, InterruptedException {
        try {
            DocumentReference docRef = db.collection(collection).document(document);    // Create a reference to a document
            ApiFuture<DocumentSnapshot> future = docRef.get();  // Get the document
            return future.get();    // Return the document
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    public boolean authenticateUser(String username, String password) throws ExecutionException, InterruptedException {
        DocumentSnapshot document = db.collection("users").document(username).get().get();  // Get the user document
        if (document.exists()) {    // Check if the document exists
            String storedPassword = document.getString("password");     // Get the stored password
            return password.equals(storedPassword);   // Compare the passwords
        }
        return false;
    }

    // public void asach(){
    //     Bucket bucket = FirestoreClient.getFirestore().getOptions().getStorageBucket();
    //     FileChooser fileChooser = new FileChooser();
    // }
}