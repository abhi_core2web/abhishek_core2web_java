package com.nevigation.DataAccess;

import com.nevigation.Controller.FormNevigationApp;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FormPage2 {
    private FormNevigationApp app;
    private GridPane root;
    private TextField field2;

    public FormPage2(FormNevigationApp app) {
        this.app = app;
        initialize();
    }

    public void initialize() {
        root = new GridPane();
        root.setPadding(new Insets(10));
        root.setHgap(10);
        root.setVgap(10);

        Label label2 = new Label("Screen 2");
        field2 = new TextField();

        Button back = new Button("Back");
        back.setOnAction(e -> {
            app.navigateToPage1();
        });

        Button next = new Button("Next");
        next.setOnAction(e -> {
            app.navigateToPage3();
        });

        Button clear = new Button("Clear");
        clear.setOnAction(e -> {
            setField2Value(null);
        });

        root.add(label2, 0, 0);
        root.add(field2, 1, 0);
        root.add(back, 0, 1);
        root.add(next, 1, 1);
        root.add(clear, 2, 1);
    }

    public GridPane getView() {
        return root;
    }

    public String getField2Value() {
        return field2.getText();
    }

    public void setField2Value(String value) {
        field2.setText(value);
    }
}
