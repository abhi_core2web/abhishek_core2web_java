package com.nevigation.DataAccess;

import com.nevigation.Controller.FormNevigationApp;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FormPage3 {
    private FormNevigationApp app;
    private GridPane root;
    private TextField field3;

    public FormPage3(FormNevigationApp app) {
        this.app = app;
        initialize();
    }

    public void initialize() {
        root = new GridPane();
        root.setHgap(10);
        root.setVgap(10);
        root.setPadding(new Insets(10));

        field3 = new TextField();
        Button back = new Button("Back");
        back.setOnAction(e -> {
            app.navigateToPage2();
        });

        Button clear = new Button("Clear");
        clear.setOnAction(e -> {
            setField3Value(null);
        });

        root.add(field3, 0, 0);
        root.add(back, 0, 1);
        root.add(clear, 1, 1);
    }

    public GridPane getView() {
        return root;
    }

    public String getField3Value() {
        return field3.getText();
    }

    public void setField3Value(String value) {
        field3.setText(value);
    }
}
