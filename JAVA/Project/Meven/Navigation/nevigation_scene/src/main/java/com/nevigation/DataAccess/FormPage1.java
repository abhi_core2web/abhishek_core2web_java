package com.nevigation.DataAccess;

import com.nevigation.Controller.FormNevigationApp;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FormPage1 {
    private FormNevigationApp app;
    private GridPane root;
    private TextField field1;

    public FormPage1(FormNevigationApp app) {
        this.app = app;
        initialize();
    }

    public void initialize() {
        root = new GridPane();

        Label label1 = new Label("Screen 1");
        field1 = new TextField();

        Button nextButton = new Button("Next");
        nextButton.setOnAction(e -> {
            app.navigateToPage2();
        });

        Button clear = new Button("Clear");
        clear.setOnAction(e -> {
            setField1Value(null);
        });

        root.add(label1, 0, 0);
        root.add(field1, 1, 1);
        root.add(nextButton, 1, 2);
        root.add(clear, 2, 2);
    }

    public GridPane getView() {
        return root;
    }

    public String getField1Value() {
        return field1.getText();
    }

    public void setField1Value(String value) {
        field1.setText(value);
    }
}
