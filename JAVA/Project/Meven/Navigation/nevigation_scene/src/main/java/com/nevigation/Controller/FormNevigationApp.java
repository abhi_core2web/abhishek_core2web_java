package com.nevigation.Controller;

import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.application.Application;

import com.nevigation.DataAccess.FormPage1;
import com.nevigation.DataAccess.FormPage2;
import com.nevigation.DataAccess.FormPage3;

public class FormNevigationApp extends Application {
    private Stage stage;

    private Scene scene1, scene2, scene3;

    private FormPage1 page1;
    private FormPage2 page2;
    private FormPage3 page3;

    @Override
    public void start(Stage stage) {
        this.stage = stage;

        page1 = new FormPage1(this);    //1000
        page2 = new FormPage2(this);    //1000
        page3 = new FormPage3(this);    //1000

        scene1 = new Scene(page1.getView(), 300, 200);
        scene2 = new Scene(page2.getView(), 300, 200);
        scene3 = new Scene(page3.getView(), 300, 200);

        stage.setScene(scene1);
        stage.setTitle("Form Navigation App");
        stage.show();
    }

    public void navigateToPage1() {
        page1.setField1Value(page1.getField1Value());
        stage.setScene(scene1);
    }
                                                            
    public void navigateToPage2() {
        page1.setField1Value(page1.getField1Value());       //obj.setName(obj.getName());
        page3.setField3Value(page3.getField3Value());
        stage.setScene(scene2);
    }

    public void navigateToPage3() {
        page2.setField2Value(page2.getField2Value());
        stage.setScene(scene3);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
