package com.blobexapmple.view;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files; // Corrected import for Files.readAllBytes
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class ImageLoader {
    private static final String BUCKET_NAME = "java-fx-fire-store.appspot.com"; // Remove "gs://" from the bucket name

    public static String uploadImage(String localFilePath, String uploadFileName) {
        try {
            // Correct the path to the JSON file and use GoogleCredentials for authentication
            FileInputStream serviceAccount = new FileInputStream("F:\\maven\\Storage_firebase\\storage\\src\\main\\resources\\fx-firestore.json");
            Storage storage = StorageOptions.newBuilder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build()
                    .getService();
            
            Path path = Paths.get(localFilePath);
            byte[] data = Files.readAllBytes(path); // Corrected variable name from 'bytes' to 'data'

            BlobId blobId = BlobId.of(BUCKET_NAME, uploadFileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/jpeg")    
            .setAcl(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER)))
            .build();
            storage.create(blobInfo, data); // Corrected variable name from 'bytes' to 'data'

            return "https://storage.googleapis.com/" + BUCKET_NAME + "/" + uploadFileName;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}