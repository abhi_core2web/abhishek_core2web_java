package com.blobexapmple.services;

import java.io.File;

import com.blobexapmple.view.ImageLoader;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImgageUploader extends Application{
    
    @Override
    public void start(Stage stage){

        ImageView imageView = new ImageView();
        Button uploadButton = new Button("Upload Image");

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg"));

        uploadButton.setOnAction(e -> {
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                String imageUrl = ImageLoader.uploadImage(file.getPath(), file.getName());

                if(imageUrl != null){
                    System.out.println(imageUrl);
                    Image image = new Image(imageUrl);
                    imageView.setImage(image);
                } else {
                    System.out.println("Image upload failed");
                }
            }
        });

        VBox root = new VBox(10,uploadButton,imageView);
        Scene scene = new Scene(root,1200,900);

        stage.setScene(scene);
        stage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
