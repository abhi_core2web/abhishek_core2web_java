package com.apibinding.service;



import java.io.IOException;

import org.json.JSONObject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

public class APIservice extends Application{
    
    // String to store the image URL
    static String imgURL = "";

    // Method to GET the image data from the API , assign the value to imgURL
    public static void imageData() throws IOException{

        // Calling method DataURL's class to get the image
        StringBuffer response = new DataUrls().getResponseData();
        if(response!=null){
            // Parse the JSON response tp get the image URL
            JSONObject obj = new JSONObject(response.toString());
            JSONObject urlObject = obj.getJSONObject("urls");
            imgURL = urlObject.getString("small");
        }
        else{
            System.out.println("Response is empty");
        }
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        imageData();

        Image image = new Image(imgURL);

        ImageView imageView = new ImageView(image);

        Pane imgPane = new Pane();
        imgPane.getChildren().add(imageView);

        Scene scene = new Scene(imgPane,image.getWidth(),image.getHeight());
        primaryStage.setScene(scene);

        primaryStage.setTitle("ImageView Example");

        primaryStage.show();
    }
    
}