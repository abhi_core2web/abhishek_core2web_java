package com.apibinding;

import com.apibinding.service.*;
import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(APIService.class, args);
    }
}