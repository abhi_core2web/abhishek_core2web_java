package com.apibinding.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class DataUrls {

    public String getKalkiMovieShowtimes() {
        StringBuilder result = new StringBuilder();
        try {
            // Construct the URL for SERP API endpoint to search for Kalki movie showtimes
            String apiUrl = "https://serpapi.com/search?q=kalki+movie+showtimes&engine=google&api_key=dda3d084634edf090e5bb22b3a053ffad72af792b924f90715280f537ebe8d53";
            URL url = new URL(apiUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                try (BufferedReader reader = new BufferedReader(new Input   StreamReader(conn.getInputStream()))) {
                    for (String line; (line = reader.readLine()) != null; ) {
                        result.append(line);
                    }
                }
            } else {
                System.out.println("Failed to fetch Kalki movie showtimes. Response code: " + responseCode);
                return null; // Or handle this case as needed
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null; // Or handle this case as needed
        }

        return result.toString();
    }

    public String getTheatersNearPune() {
        StringBuilder result = new StringBuilder();
        try {
            // Construct the URL for SERP API endpoint to search for theaters near Pune
            String apiUrl = "https://serpap i.com/search?q=theaters+near+pune&engine=google&api_key=dda3d084634edf090e5bb22b3a053ffad72af792b924f90715280f537ebe8d53";
            URL url = new   URL(apiUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                    for (String line; (line = reader.readLine()) != null; ) {
                        result.append(line);
                    }
                }
            } else {
                System.out.println("Failed to fetch theaters near Pune. Response code: " + responseCode);
                return null; // Or handle this case as needed
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null; // Or handle this case as needed
        }

        return result.toString();
    }
}
