package com.apibinding.test;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MovieApp extends Application {

    private static final String MOVIE_API_KEY = "6b677e84f4msh5170f2eb7a56ba2p121d16jsncbdeed1d1669"; // Replace with your actual RapidAPI key
    private static final String MOVIE_API_HOST = "movieseries.p.rapidapi.com";

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hollywood Movie Details");

        Label movieLabel = new Label("Click the button to fetch movie details.");

        Button movieBtn = new Button();
        movieBtn.setText("Get Movie Details");
        movieBtn.setOnAction(event -> {
            String movieResponse = getMovieDetails();
            movieLabel.setText(movieResponse);
        });

        VBox root = new VBox();
        root.getChildren().addAll(movieBtn, movieLabel);

        primaryStage.setScene(new Scene(root, 400, 300));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private String getMovieDetails() {
        String url = "https://movieseries.p.rapidapi.com/hollywood";

        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("x-rapidapi-key", MOVIE_API_KEY);
            con.setRequestProperty("x-rapidapi-host", MOVIE_API_HOST);

            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            } else {
                return "GET request failed: " + responseCode + " - " + con.getResponseMessage();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }
    }
}
