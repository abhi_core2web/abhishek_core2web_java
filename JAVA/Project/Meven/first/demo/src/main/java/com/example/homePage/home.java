package com.example.homePage;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class home extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("CORE2WEB");
        primaryStage.getIcons().add(new Image("assets/image/eye.png"));
        primaryStage.show();
    }
    
}
