package com.nevugation;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class SignUp {
    
    private Pane pane;
    private Login login;
    private Scene loginScene;

    public SignUp(Login login) {
        this.login = login;
        create();
    }

    public void create() {
        pane = new Pane();

        loginScene = new Scene(login.getLoginScene(), 800, 600);

        Label label = new Label("Sign Up");
        Button homeButton = new Button("Home");
        homeButton.setLayoutX(100);
        homeButton.setLayoutY(100);
        homeButton.setOnAction(e -> login.redirectLoginPage());
        pane.getChildren().addAll(label, homeButton);
    }

    public Pane getSignUpScene() {
        return pane;
    }
}
