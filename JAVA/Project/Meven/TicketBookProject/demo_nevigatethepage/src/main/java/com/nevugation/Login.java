package com.nevugation;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class Login {

    private Scene signupScene;
    private Pane pane;
    private Landing landing;

    private SignUp signup;

    public Login(Landing landing) {
        this.landing = landing;
        create();
    }

    public void create() {
        pane = new Pane();

        signup = new SignUp(this);

        Label label = new Label("Login");
        Button signup = new Button("Home");
        signup.setLayoutX(100);
        signup.setLayoutY(100);
        pane.getChildren().addAll(label, signup);

    }

    public Pane getLoginScene() {
        return pane;
    }

}
