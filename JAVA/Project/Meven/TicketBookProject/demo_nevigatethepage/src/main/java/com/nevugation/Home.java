package com.nevugation;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Home extends Application{
    
    private Stage stage;
    
    private Scene loginScene;
    private Scene homeScene;
    private Scene paymentScene;

    private Login login;
    private Payment payment;

    @Override
    public void start(Stage stage){
        this.stage = stage;
        stage.setTitle("Home");

        login = new Login(this);
        payment = new Payment(this);


        loginScene = new Scene(login.getLoginScene(), 600, 400);
        paymentScene = new Scene(payment.getPaymentScene(), 600, 400);
        
        Label label = new Label("Home");
        Pane pane = new Pane();
        Button loginButton = new Button("Login");
        loginButton.setLayoutX(100);
        loginButton.setLayoutY(100);
        loginButton.setOnAction(e -> redirectLoginPage());
        Button paymentButton = new Button("Payment");
        paymentButton.setLayoutX(200);
        paymentButton.setLayoutY(100);
        paymentButton.setOnAction(e -> redirectPaymentPage());
        pane.getChildren().addAll(label, loginButton, paymentButton);
        
        
        homeScene = new Scene(pane, 600, 400);
        stage.setScene(homeScene);
        stage.show();
    }

    public void redirectLoginPage(){
        stage.setScene(loginScene);
    }
    public void redirectHomePage(){
        stage.setScene(homeScene);
    }

    public void redirectPaymentPage(){
        stage.setScene(paymentScene);
    }
    
    
}
