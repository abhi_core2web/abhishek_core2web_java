package com.nevugation;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class Payment {
    
    private Pane pane;
    private Home home;

    public Payment(Home home) {
        this.home = home;
        create();
    }

    public void create() {
        pane = new Pane();

        Label label = new Label("Payment");
        Button homeButton = new Button("Home");
        homeButton.setLayoutX(100);
        homeButton.setLayoutY(100);
        homeButton.setOnAction(e -> home.redirectHomePage());
        pane.getChildren().addAll(label, homeButton);

    }

    public Pane getPaymentScene() {
        return pane;
    }

    
}
