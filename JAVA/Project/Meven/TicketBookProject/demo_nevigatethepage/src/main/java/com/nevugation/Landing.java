package com.nevugation;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Landing extends Application{
        
    private Stage stage;
    private Scene loginScene;
    private Pane pane;
    private Login login;

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        
        pane = new Pane();

        login = new Login(this);

        loginScene = new Scene(login.getLoginScene(), 800, 600);

        Button getStarted = new Button("Get Started");
        getStarted.setLayoutX(100);
        getStarted.setLayoutY(100);
        getStarted.setOnAction(e -> stage.setScene(loginScene));

        pane.getChildren().addAll(getStarted);
        Scene scene = new Scene(pane, 800, 600);   
        stage.setScene(scene);
        stage.show();
    }
}